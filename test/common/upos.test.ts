import { UPos, UPosFromStr, UPosToStr } from "../../src/g3";

test("String to UPos enum conversion.", () => {
    const upos = UPos.ADJ;
    expect(UPosFromStr("ADJ")).toBe(upos);
    expect(UPosFromStr("adj")).toBe(upos);
    expect(UPosFromStr("Adj")).toBe(upos);
    expect(UPosFromStr("unknwon")).toBe(UPos.X);
    expect(UPosFromStr("")).toBe(UPos.X);
});

test("UPos enum to string conversion.", () => {
    expect(UPosToStr(UPos.SCONJ)).toBe("SCONJ");
});
