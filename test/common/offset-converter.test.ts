import { CharSpan } from "../../src/common/char-span";
import {
    Cp2JSOffsetConverter,
    JS2CpOffsetConverter,
} from "../../src/common/offset-converter";

test("Codepoint offsets to JavaScript string character offsets.", () => {
    const text = "V 🐦🎾 obchodě chybí nová alba 🤣.";
    const spans = [
        [0, "V"],
        [2, "🐦🎾"],
        [5, "obchodě"],
        [13, "chybí"],
        [19, "nová"],
        [24, "alba"],
        [29, "🤣"],
        [30, "."],
    ];

    const converter = new Cp2JSOffsetConverter(text);
    for (const span of spans) {
        const length = (span[1] as string).length;
        const charOffset = converter.convert(span[0] as number);
        const charSpan = CharSpan.withLen(charOffset, length);

        expect(span[1] === charSpan.extractText(text)).toBeTruthy();
    }
});

test("Offset conversion from codepoints to JavaScript string indices and back.", () => {
    const text = "V 🐦🎾 obchodě chybí nová alba 🤣.";
    const origSpans = [
        [0, 1, "V"],
        [2, 4, "🐦🎾"],
        [5, 12, "obchodě"],
        [13, 18, "chybí"],
        [19, 23, "nová"],
        [24, 28, "alba"],
        [29, 30, "🤣"],
        [30, 31, "."],
    ];

    const converterToJS = new Cp2JSOffsetConverter(text);
    const converterToCp = new JS2CpOffsetConverter(text);

    const charSpans = origSpans.map((s) => {
        const charOffset = converterToJS.convert(s[0] as number);
        return CharSpan.withLen(charOffset, (s[2] as string).length);
    });

    const cpSpans = charSpans.map((cs) => {
        const cpStartOffset = converterToCp.convert(cs.start);
        const cpEndOffset = converterToCp.convert(cs.end);
        return [cpStartOffset, cpEndOffset];
    });

    for (let i = 0; i < charSpans.length; i++) {
        expect(origSpans[i][0] === cpSpans[i][0]).toBeTruthy();
        expect(origSpans[i][1] === cpSpans[i][1]).toBeTruthy();
    }
});
