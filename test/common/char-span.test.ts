import { CharSpan } from "../../src/common/char-span";

test("CharSpan length.", () => {
    expect(new CharSpan(4, 8).length).toBe(4);
    expect(new CharSpan(10, 11).length).toBe(1);
});

test("CharSpan illegal arguments for of()", () => {
    expect(() => CharSpan.of(4, 4)).toThrow();
    expect(() => CharSpan.of(4, 3)).toThrow();
    expect(() => CharSpan.of(4, -1)).toThrow();
    expect(() => CharSpan.of(-1, 3)).toThrow();
    expect(() => CharSpan.of(-1, -1)).toThrow();
});

test("CharSpan illegal arguments for withLen().", () => {
    expect(() => CharSpan.withLen(-1, 2)).toThrow();
    expect(() => CharSpan.withLen(1, -2)).toThrow();
    expect(() => CharSpan.withLen(-1, -2)).toThrow();
    expect(() => CharSpan.withLen(4, 0)).toThrow();
});

test("CharSpan extractText()", () => {
    expect(new CharSpan(2, 5).extractText("abcdefgh")).toBe("cde");
    expect(new CharSpan(2, 3).extractText("abcdefgh")).toBe("c");
    expect(new CharSpan(0, 3).extractText("abcdefgh")).toBe("abc");
    expect(new CharSpan(0, 1).extractText("abcdefgh")).toBe("a");
    expect(new CharSpan(2, 4).extractText("abcd")).toBe("cd");
    expect(new CharSpan(3, 4).extractText("abcd")).toBe("d");
});

test("CharSpan is outside of the text.", () => {
    expect(() => new CharSpan(3, 6).extractText("abcd")).toThrow();
});

test("Consistency between of() and withLen()", () => {
    expect(
        CharSpan.of(0, 2).extractText("abcd") ===
            CharSpan.withLen(0, 2).extractText("abcd"),
    ).toBeTruthy();
    expect(
        CharSpan.of(2, 4).extractText("abcde") ===
            CharSpan.withLen(2, 2).extractText("abcde"),
    ).toBeTruthy();
});
