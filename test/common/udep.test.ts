import { UDep, UDepFromStr, UDepToStr } from "../../src/g3";

test("String to UDep enum conversion.", () => {
    const udep = UDep.DET;
    expect(UDepFromStr("DET")).toBe(udep);
    expect(UDepFromStr("det")).toBe(udep);
    expect(UDepFromStr("Det")).toBe(udep);
    expect(UDepFromStr("")).toBe(UDep.DEP);
    expect(UDepFromStr("unknown")).toBe(UDep.DEP);
});

test("UDep enum to string conversion.", () => {
    expect(UDepToStr(UDep.NSUBJPASS)).toBe("nsubjpass");
});
