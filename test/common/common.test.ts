import { isSequential, isSorted } from "../../src/common/common";

test("isSequential", () => {
    expect(isSequential([0, 1, 2, 3, 4])).toBeTruthy();
    expect(isSequential([-3, -2, -1, 0, 1, 2, 3, 4])).toBeTruthy();
    expect(isSequential([0])).toBeTruthy();
    expect(isSequential([5])).toBeTruthy();
    expect(isSequential([])).toBeTruthy();

    expect(isSequential([1, 2, 4, 5])).toBeFalsy();
    expect(isSequential([1, 1])).toBeFalsy();
    expect(isSequential([2, 1])).toBeFalsy();
});

test("isSorted", () => {
    expect(isSorted([1, 2, 3])).toBeTruthy();
    expect(isSorted([1, 2, 3, 5, 6])).toBeTruthy();
    expect(isSorted([1, 2, 2, 2, 3])).toBeTruthy();
    expect(isSorted([1, 1])).toBeTruthy();
    expect(isSorted([-3, 0, 2, 3])).toBeTruthy();
    expect(isSorted([1])).toBeTruthy();
    expect(isSorted([])).toBeTruthy();

    expect(isSorted([1, 2, 3, 0])).toBeFalsy();
});
