import { SemVer } from "../../src/common/sem-ver";

test("SemVer comparisons.", () => {
    const version = new SemVer(3, 2, 1);
    expect(version.isSameAs(new SemVer(3, 2, 1))).toBeTruthy();
    expect(version.isSameAs(new SemVer(3, 2, 0))).toBeFalsy();
    expect(version.isOlderThan(new SemVer(3, 2, 2))).toBeTruthy();
    expect(version.isOlderThan(new SemVer(3, 3, 1))).toBeTruthy();
    expect(version.isOlderThan(new SemVer(4, 2, 1))).toBeTruthy();
    expect(version.isOlderThan(new SemVer(3, 2, 0))).toBeFalsy();
    expect(version.isOlderThan(new SemVer(3, 2, 1))).toBeFalsy();
    expect(version.compareTo(new SemVer(3, 0, 0))).toBeGreaterThan(0);
});

test("String to SemVer conversions.", () => {
    expect(SemVer.valueOf("3.2.1").isSameAs(new SemVer(3, 2, 1))).toBeTruthy();
    expect(() => SemVer.valueOf("3.2.1.0")).toThrow();
    expect(() => SemVer.valueOf("not a version at all")).toThrow();
    expect(() => SemVer.valueOf("3.NaN.1")).toThrow();
});
