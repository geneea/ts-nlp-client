import * as examples from "../examples";
import { CharSpan } from "../../../src/common/char-span";
import { readFromJson, TokenSupport } from "../../../src/g3";
import { buildTestStrTree } from "./tree-builder.test";

const tree = buildTestStrTree(
    ["a", "b", "c", "d", "e", "f"],
    [
        [0, 1],
        [1, 2],
        [2, 3],
        [3, 4],
        [4, 5],
    ],
);

test("a", () => {
    // [a] b c d e f
    //  0 12345678901
    const sup = TokenSupport.of([tree.tokens[0]]);
    expect(sup.size).toBe(1);
    expect(sup.isContinuous).toBeTruthy();

    const spans = sup.spans();
    expect([sup]).toEqual(Array.from(spans));

    expect(sup.first === tree.tokens[0]).toBeTruthy();
    expect(sup.last === tree.tokens[0]).toBeTruthy();

    expect(sup.firstCharParaOffset).toBe(0);
    expect(sup.lastCharParaOffset).toBe(1);
    expect(sup.charSpan).toEqual(new CharSpan(0, 1));
});

test("bcd", () => {
    // a [b c d] e f
    // 01 23456 78901
    const sup = TokenSupport.of([
        tree.tokens[1],
        tree.tokens[2],
        tree.tokens[3],
    ]);
    expect(sup.size).toBe(3);
    expect(sup.isContinuous).toBeTruthy();

    const spans = sup.spans();
    expect([sup]).toEqual(Array.from(spans));

    expect(sup.first === tree.tokens[1]).toBeTruthy();
    expect(sup.last === tree.tokens[3]).toBeTruthy();

    expect(sup.firstCharParaOffset).toBe(2);
    expect(sup.lastCharParaOffset).toBe(7);
    expect(sup.charSpan).toEqual(new CharSpan(2, 7));
});

test("b de", () => {
    // a [b] c [d e] f
    // 01 2 345 678 901
    const sup = TokenSupport.of([
        tree.tokens[1],
        tree.tokens[3],
        tree.tokens[4],
    ]);
    expect(sup.size).toBe(3);
    expect(sup.isContinuous).toBeFalsy();

    const spans = Array.from(sup.spans());
    expect(spans.length).toEqual(2);
    expect([tree.tokens[1]]).toEqual(spans[0].tokens);
    expect([tree.tokens[3], tree.tokens[4]]).toEqual(spans[1].tokens);

    expect(sup.first === tree.tokens[1]).toBeTruthy();
    expect(sup.last === tree.tokens[4]).toBeTruthy();

    expect(sup.firstCharParaOffset).toBe(2);
    expect(sup.lastCharParaOffset).toBe(9);
    expect(sup.charSpan).toEqual(new CharSpan(2, 9));
});

test("Check", () => {
    expect(() => TokenSupport.of([])).toThrow();

    const obj = readFromJson(examples.exampleAnalysis());
    // para[1] "Angela Merkel left Germany. She move to New Orleans to learn jazz. That's amazing."

    // token support accross two sentences
    let tokLen = obj.paragraphs[1].sentences[0].tokens.length;
    expect(() =>
        TokenSupport.of([
            obj.paragraphs[1].sentences[0].tokens[tokLen - 1],
            obj.paragraphs[1].sentences[1].tokens[0],
        ]),
    ).toThrow();

    tokLen = obj.paragraphs[0].sentences[0].tokens.length;
    expect(() =>
        TokenSupport.of([
            obj.paragraphs[0].sentences[0].tokens[tokLen - 1],
            obj.paragraphs[1].sentences[0].tokens[0],
        ]),
    ).toThrow();
});

test("Text", () => {
    const obj = readFromJson(examples.exampleAnalysis());
    // para[1] "Angela Merkel left Germany. She move to New Orleans to learn jazz. That's amazing."

    const tokens = obj.paragraphs[1].sentences[0].tokens;
    let sup = TokenSupport.of(tokens.slice(0, 1));
    expect(sup.text).toBe("Angela");
    expect(sup.textSpans()).toEqual(["Angela"]);

    sup = TokenSupport.of(tokens.slice(2, 3));
    expect(sup.text).toBe("left");
    expect(sup.textSpans()).toEqual(["left"]);

    sup = TokenSupport.of(tokens.slice(0, 3));
    expect(sup.text).toBe("Angela Merkel left");
    expect(sup.textSpans()).toEqual(["Angela Merkel left"]);

    sup = TokenSupport.of(tokens.slice(0, 1).concat(tokens.slice(2, 3)));
    expect(sup.text).toBe("Angela Merkel left");
    expect(sup.textSpans()).toEqual(["Angela", "left"]);
});
