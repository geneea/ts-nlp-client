import { GkbProperty } from "../../../src/g3/model/gkb-propert";

test("Gkb property has to have one value", () => {
    expect(() => new GkbProperty("name", "label")).toThrow();
    expect(new GkbProperty("name", "label", null, true)).toBeDefined();
});
