import { buildIdxTree, buildTestStrTree } from "./tree-builder.test";

test("In-order tree traversing.", () => {
    let tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 1],
            [1, 2],
            [2, 3],
            [3, 4],
            [4, 5],
        ],
    );
    let received = Array.from(tree.root.inOrder()).map((t) => t.text);
    expect(received).toEqual(["0", "1", "2", "3", "4", "5"]);

    tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 4],
            [1, 4],
            [2, 4],
            [3, 4],
            [4, 5],
        ],
    );
    received = Array.from(tree.root.inOrder()).map((t) => t.text);
    expect(received).toEqual(["0", "1", "2", "3", "4", "5"]);

    tree = buildTestStrTree(
        ["0", "1", "2", "3", "4", "5"],
        [
            [0, 2],
            [2, 3],
            [3, 4],
            [4, 5],
            [1, 5],
        ],
    );
    received = Array.from(tree.root.inOrder()).map((t) => t.text);
    expect(received).toEqual(["1", "0", "2", "3", "4", "5"]);
});

test("Filtered in-order tree traversing.", () => {
    let tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 1],
            [1, 2],
            [2, 3],
            [3, 4],
            [4, 5],
        ],
    );
    let received = Array.from(
        tree.root.filteredInOrder(true, (t) => t.text === "1"),
    ).map((t) => t.text);
    expect(received).toEqual(["1", "2", "3", "4", "5"]);

    received = Array.from(
        tree.root.filteredInOrder(false, (t) => t.text === "1"),
    ).map((t) => t.text);
    expect(received).toEqual(["2", "3", "4", "5"]);

    tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 4],
            [1, 4],
            [2, 4],
            [3, 4],
            [4, 5],
        ],
    );
    received = Array.from(
        tree.root.filteredInOrder(true, (t) => t.text === "1"),
    ).map((t) => t.text);
    expect(received).toEqual(["0", "1", "2", "3", "4", "5"]);

    received = Array.from(
        tree.root.filteredInOrder(true, (t) => t.text === "4"),
    ).map((t) => t.text);
    expect(received).toEqual(["4", "5"]);

    received = Array.from(
        tree.root.filteredInOrder(false, (t) => t.text === "4"),
    ).map((t) => t.text);
    expect(received).toEqual(["5"]);
});

test("Pre-order tree traversing.", () => {
    let tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 1],
            [1, 2],
            [2, 3],
            [3, 4],
            [4, 5],
        ],
    );
    let received = Array.from(tree.root.preOrder()).map((t) => t.text);
    expect(received).toEqual(["5", "4", "3", "2", "1", "0"]);

    tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 4],
            [1, 4],
            [2, 4],
            [3, 4],
            [4, 5],
        ],
    );
    received = Array.from(tree.root.preOrder()).map((t) => t.text);
    expect(received).toEqual(["5", "4", "0", "1", "2", "3"]);

    tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 2],
            [2, 3],
            [3, 4],
            [4, 5],
            [1, 5],
        ],
    );
    received = Array.from(tree.root.preOrder()).map((t) => t.text);
    expect(received).toEqual(["5", "1", "4", "3", "2", "0"]);
});

test("Filtered pre-order tree traversing.", () => {
    let tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 1],
            [1, 2],
            [2, 3],
            [3, 4],
            [4, 5],
        ],
    );
    let received = Array.from(
        tree.root.filteredPreOrder(true, (t) => t.text === "1"),
    ).map((t) => t.text);
    expect(received).toEqual(["5", "4", "3", "2", "1"]);

    received = Array.from(
        tree.root.filteredPreOrder(false, (t) => t.text === "1"),
    ).map((t) => t.text);
    expect(received).toEqual(["5", "4", "3", "2"]);

    tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 4],
            [1, 4],
            [2, 4],
            [3, 4],
            [4, 5],
        ],
    );
    received = Array.from(
        tree.root.filteredPreOrder(true, (t) => t.text === "1"),
    ).map((t) => t.text);
    expect(received).toEqual(["5", "4", "0", "1", "2", "3"]);

    received = Array.from(
        tree.root.filteredPreOrder(true, (t) => t.text === "4"),
    ).map((t) => t.text);
    expect(received).toEqual(["5", "4"]);

    received = Array.from(
        tree.root.filteredPreOrder(false, (t) => t.text === "4"),
    ).map((t) => t.text);
    expect(received).toEqual(["5"]);
});
