import { CharSpan } from "../../../src/common/char-span";
import {
    Sentence,
    Token,
    Tree,
    TreeBuilder,
    UDep,
    UPos,
} from "../../../src/g3";

function pretendToken(idx: number, text: string, charStart = 0): Token {
    return Token.of(
        `t${idx}`,
        idx,
        text,
        CharSpan.withLen(charStart, text.length),
        null,
        null,
        `DL${text}`,
        `L${text}`,
        UPos.X,
        "X",
        null,
        UDep.DEP,
    );
}

function buildTestTree(tokens: Token[], deps: [number, number][]): Tree<Token> {
    const builder = new TreeBuilder<Token>();
    for (const tok of tokens) {
        builder.addNode(tok);
    }
    for (const dep of deps) {
        builder.addDependency(dep[0], dep[1]);
    }

    const tree = builder.build() as Tree<Token>;
    const sentence = Sentence.of("id0", tree.root, tree.tokens);
    tree.tokens.forEach((t) => (t.sentence = sentence));
    return tree;
}

/** Tokens get valid indices and space-separated spans.  */
export function buildTestStrTree(
    tokenStrs: string[],
    deps: [number, number][],
): Tree<Token> {
    let charOffset = 0;

    const tokens = new Array<Token>();
    tokenStrs.forEach((text, idx) => {
        tokens.push(pretendToken(idx, text, charOffset));
        charOffset += text.length + 1;
    });

    return buildTestTree(tokens, deps);
}

/** For testing indices, token spans are not valid.  */
export function buildIdxTree(
    idx: number[],
    deps: [number, number][],
): Tree<Token> {
    return buildTestTree(
        idx.map((i) => pretendToken(i, i.toString())),
        deps,
    );
}

test("Build a tree with 6 tokens dependent on the last one.", () => {
    const tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 5],
            [1, 5],
            [2, 5],
            [3, 5],
            [4, 5],
        ],
    );

    for (let i = 0; i < 6; i++) {
        expect(tree.tokens[i].idx).toBe(i);
    }

    expect(tree.root === tree.tokens[5]).toBeTruthy();
    expect(tree.root.parent).toBeNull();

    for (const tok of tree.tokens.slice(0, 5)) {
        expect(tok.parent === tree.root).toBeTruthy();
        expect(tok.children).toEqual([]);
    }
});

test("Build a tree with 6 tokens each dependent on the previous one.", () => {
    const tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [1, 0],
            [2, 1],
            [3, 2],
            [4, 3],
            [5, 4],
        ],
    );

    for (let i = 0; i < 6; i++) {
        expect(tree.tokens[i].idx).toBe(i);
    }

    expect(tree.root === tree.tokens[0]).toBeTruthy();
    expect(tree.root.parent).toBeNull();

    expect(tree.tokens[0].parent).toBeNull();
    expect(tree.tokens[1].parent === tree.tokens[0]).toBeTruthy();
    expect(tree.tokens[2].parent === tree.tokens[1]).toBeTruthy();
    expect(tree.tokens[3].parent === tree.tokens[2]).toBeTruthy();
    expect(tree.tokens[4].parent === tree.tokens[3]).toBeTruthy();
    expect(tree.tokens[5].parent === tree.tokens[4]).toBeTruthy();

    expect(tree.tokens[0].children).toEqual([tree.tokens[1]]);
    expect(tree.tokens[1].children).toEqual([tree.tokens[2]]);
    expect(tree.tokens[2].children).toEqual([tree.tokens[3]]);
    expect(tree.tokens[3].children).toEqual([tree.tokens[4]]);
    expect(tree.tokens[4].children).toEqual([tree.tokens[5]]);
    expect(tree.tokens[5].children).toEqual([]);
});

test("Build a tree with dummy dependencies.", () => {
    const builder = new TreeBuilder<Token>();

    for (let i = 0; i < 6; i++) {
        builder.addNode(pretendToken(i, i.toString()));
    }
    builder.addDummyDependencies();

    const tree = builder.build() as Tree<Token>;
    tree.tokens.forEach((tok, i) => expect(tok.idx).toBe(i));
    expect(tree.root === tree.tokens[0]).toBeTruthy();
    expect(tree.root.parent).toBeNull();

    expect(tree.tokens[0].parent).toBeNull();
    expect(tree.tokens[1].parent == tree.tokens[0]).toBeTruthy();
    expect(tree.tokens[2].parent == tree.tokens[0]).toBeTruthy();
    expect(tree.tokens[3].parent == tree.tokens[0]).toBeTruthy();
    expect(tree.tokens[4].parent == tree.tokens[0]).toBeTruthy();
    expect(tree.tokens[5].parent == tree.tokens[0]).toBeTruthy();

    expect(tree.tokens[0].children).toEqual([
        tree.tokens[1],
        tree.tokens[2],
        tree.tokens[3],
        tree.tokens[4],
        tree.tokens[5],
    ]);

    expect(tree.tokens[1].children).toEqual([]);
    expect(tree.tokens[2].children).toEqual([]);
    expect(tree.tokens[3].children).toEqual([]);
    expect(tree.tokens[4].children).toEqual([]);
    expect(tree.tokens[5].children).toEqual([]);
});

test("Build nonprojective tree.", () => {
    const tree = buildIdxTree(
        [0, 1, 2, 3, 4, 5],
        [
            [0, 1],
            [1, 5],
            [2, 3],
            [3, 4],
            [5, 4],
        ],
    );
    tree.tokens.forEach((tok, i) => expect(tok.idx).toBe(i));
});

test("Nonconsecutive token indexes.", () => {
    expect(() =>
        buildIdxTree(
            [0, 1, 2, 4, 5],
            [
                [0, 5],
                [1, 5],
                [2, 5],
                [4, 5],
            ],
        ),
    ).toThrow();
});

test("First token with non-zero index.", () => {
    expect(() =>
        buildIdxTree(
            [1, 2, 3, 4, 5],
            [
                [1, 5],
                [2, 5],
                [3, 5],
                [4, 5],
            ],
        ),
    ).toThrow();
});

test("Tree with multiple roots.", () => {
    expect(() =>
        buildIdxTree(
            [0, 1, 2, 3, 4, 5],
            [
                [0, 2],
                [1, 2],
                [3, 5],
                [4, 5],
            ],
        ),
    ).toThrow();
});

test("Circular edges.", () => {
    expect(() =>
        buildIdxTree(
            [0, 1, 2, 3, 4, 5],
            [
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [4, 2],
            ],
        ),
    ).toThrow();
});
