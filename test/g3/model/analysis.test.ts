import { Paragraph, readFromJson } from "../../../src/g3";
import { exampleAnalysis } from "../examples";

const analysis = readFromJson(exampleAnalysis());

test("Analysis' tokens' offset tokens.", () => {
    expect(analysis.paragraphs[0].sentences[0].tokens[0]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[0].offsetToken(0),
    );

    // non-existing offset yields null
    expect(
        analysis.paragraphs[0].sentences[0].tokens[0].offsetToken(-1),
    ).toBeNull();
    expect(
        analysis.paragraphs[0].sentences[0].tokens[0].offsetToken(-10),
    ).toBeNull();
    expect(
        analysis.paragraphs[0].sentences[0].tokens[1].offsetToken(-2),
    ).toBeNull();
    expect(
        analysis.paragraphs[0].sentences[0].tokens[1].offsetToken(-10),
    ).toBeNull();

    expect(analysis.paragraphs[0].sentences[0].tokens[0]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[1].offsetToken(-1),
    );
    expect(analysis.paragraphs[0].sentences[0].tokens[1]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[2].offsetToken(-1),
    );
    expect(analysis.paragraphs[0].sentences[0].tokens[0]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[2].offsetToken(-2),
    );

    // non-existing offset yields null
    const len = analysis.paragraphs[0].sentences[0].tokens.length;
    expect(
        analysis.paragraphs[0].sentences[0].tokens[len - 1].offsetToken(1),
    ).toBeNull();
    expect(
        analysis.paragraphs[0].sentences[0].tokens[len - 1].offsetToken(10),
    ).toBeNull();
    expect(
        analysis.paragraphs[0].sentences[0].tokens[len - 2].offsetToken(2),
    ).toBeNull();
    expect(
        analysis.paragraphs[0].sentences[0].tokens[len - 2].offsetToken(10),
    ).toBeNull();

    expect(analysis.paragraphs[0].sentences[0].tokens[1]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[0].offsetToken(1),
    );
    expect(analysis.paragraphs[0].sentences[0].tokens[2]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[0].offsetToken(2),
    );
    expect(analysis.paragraphs[0].sentences[0].tokens[2]).toBe(
        analysis.paragraphs[0].sentences[0].tokens[1].offsetToken(1),
    );
});

test("Analysis' tokens' charspan's extractText().", () => {
    const p0 = analysis.paragraphs[0];
    const p1 = analysis.paragraphs[1];

    expect(p0.sentences[0].tokens[0].charSpan.extractText(p0.text)).toBe(
        "Angela",
    );
    expect(p0.sentences[0].tokens[3].charSpan.extractText(p0.text)).toBe("New");
    expect(p1.sentences[0].tokens[0].charSpan.extractText(p1.text)).toBe(
        "Angela",
    );
    expect(p1.sentences[0].tokens[3].charSpan.extractText(p1.text)).toBe(
        "Germany",
    );
    expect(p1.sentences[2].tokens[0].charSpan.extractText(p1.text)).toBe(
        "That",
    );
    expect(p1.sentences[2].tokens[2].charSpan.extractText(p1.text)).toBe(
        "amazing",
    );
});

test("Analysis' getParaByType().", () => {
    expect(analysis.paragraphs[0]).toEqual(
        analysis.getParaByType(Paragraph.TYPE_TITLE),
    );
    expect(analysis.paragraphs[1]).toEqual(
        analysis.getParaByType(Paragraph.TYPE_BODY),
    );
});

test("Analysis' language.", () => {
    expect(analysis.language.detected).toBe("en");
});
