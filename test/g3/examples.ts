/* eslint-disable @typescript-eslint/no-explicit-any */
import * as fs from "fs";

export function readAndParseUTF8Json(filePath: string): string {
    filePath = "test/resources/" + filePath;
    return JSON.parse(fs.readFileSync(filePath, "utf-8"));
}

export const allAnalysisExamples: () => any[] = () => {
    const examples = fs
        .readdirSync("test/resources")
        .filter((f) => f.startsWith("example", 0));

    examples.push("article_example.analysis.json");

    return examples.map((f) => readAndParseUTF8Json(f));
};

export const exampleAnalysis: () => any = () =>
    readAndParseUTF8Json("example.json");

export const exampleAnalysisFull: () => any = () =>
    readAndParseUTF8Json("example_FULL.json");

export const exampleAnalysisV321: () => any = () =>
    readAndParseUTF8Json("G3v321_example_TOKENS.json");

export const exampleAnalysisV320: () => any = () =>
    readAndParseUTF8Json("G3v320_example_TOKENS.json");

export const exampleAnalysisV311: () => any = () =>
    readAndParseUTF8Json("G3v311_example_TOKENS.json");

export const exampleAnalysisArticle: () => any = () =>
    readAndParseUTF8Json("article_example.analysis.json");

export const exampleAnalysisBugFixSDK41: () => any = () =>
    readAndParseUTF8Json("bugfix_example.SDK-41.json");

export const exampleAnalysisBugFixSDK44: () => any = () =>
    readAndParseUTF8Json("bugfix_example.SDK-44.json");

export const exampleAnalysisBugFixSDK45: () => any = () =>
    readAndParseUTF8Json("bugfix_example.SDK-45.json");

export const exampleAnalysisBugFixSDK46: () => any = () =>
    readAndParseUTF8Json("bugfix_example.SDK-46.json");

export const exampleTagsWithProps: () => any = () =>
    readAndParseUTF8Json("example_TAGS_PROPS.json");

export const exampleEntitiesWithProps: () => any = () =>
    readAndParseUTF8Json("example_ENTITIES_PROPS.json");

export const exampleRequest: () => any = () =>
    readAndParseUTF8Json("request.json");
