/* eslint-disable @typescript-eslint/no-explicit-any */
import * as examples from "../examples";
import {
    readFromJson,
    Sentiment,
    Token,
    UDep,
    writeToJson,
} from "../../../src/g3";
import { GkbProperty } from "../../../src/g3/model/gkb-propert";

function _replaceClauseByRoot(rawJson: any): void {
    rawJson.paragraphs?.forEach((p: { sentences: any[] }) =>
        p?.sentences.forEach((s: { tokens: any[] }) =>
            s?.tokens.forEach((t: { fnc: string }) => {
                if (t.fnc === "clause") t.fnc = "root";
            }),
        ),
    );
}

test("Testing whether reading and subsequential writing produces identical JSON strings.", () => {
    examples.allAnalysisExamples().forEach((rawJson) => {
        _replaceClauseByRoot(rawJson);

        if (
            rawJson.itemSentiments &&
            Object.keys(rawJson.itemSentiments).length === 0
        )
            delete rawJson.itemSentiments;

        const analysis = readFromJson(rawJson);
        const output = writeToJson(analysis);

        expect(output).toEqual(rawJson);
    });
});

test("Reading newer G3 with the same major version and write it again. It is assumed that after write some content can be lost.", () => {
    const json = examples.readAndParseUTF8Json(
        "forward_example_ENTITIES_PROPS.json",
    );
    const actual = writeToJson(readFromJson(json));
    const expected = examples.readAndParseUTF8Json(
        "example_ENTITIES_PROPS.json",
    );

    expect(actual).toEqual(expected);
});

test("Reading tokens from JSON.", () => {
    const jsonAnalysis = examples.exampleAnalysisFull();
    const analysis = readFromJson(jsonAnalysis);

    const t0 = analysis.paragraphs[0].sentences[0].tokens[0]; // Angela

    expect(t0.id).toBe("w0");
    expect(t0.text).toBe("Angela");
    expect(t0.charSpan.start).toBe(0);
    expect(t0.charSpan.end).toBe(6);
    expect(t0.origText).toBe("Angela");
    expect(t0.origCharSpan.start).toBe(0);
    expect(t0.origCharSpan.end).toBe(6);
    expect(t0.deepLemma).toBe("Angela");
    expect(t0.lemma).toBeNull();
    expect(t0.morphTag).toBe("NNP");
    expect(t0.fnc).toBe(UDep.COMPOUND);
    expect(t0.feats.get("negated")).toBe("false");

    expect(t0.parent!.text).toBe("Merkel");
    expect(t0.children).toEqual([]);
    expect(t0.previous()).toBeNull();
    expect(t0.next()!.text).toBe("Merkel");
});

test("Reading tecto tokens from JSON.", () => {
    const jsonAnalysis = examples.exampleAnalysisFull();
    const analysis = readFromJson(jsonAnalysis);

    const d0 = analysis.paragraphs[0].sentences[0].tectoTokens![0]; // root
    const d1 = analysis.paragraphs[0].sentences[0].tectoTokens![1]; // Angela Merkel
    const w0 = analysis.paragraphs[0].sentences[0].tokens[0]; // Angela
    const w1 = analysis.paragraphs[0].sentences[0].tokens[1]; // Merkel

    expect(d1.id).toBe("d1");
    expect(d1.lemma).toBe("Angela Merkel");
    expect(d1.parent).toBe(d0);
    expect(d1.fnc).toBe("clause");
    expect(d1.tokens!.tokens).toEqual([w0, w1]);
});

test("Consistency between origText and corrText version.", () => {
    const _test = (toks: Token[]) => {
        expect(toks[3].text).toBe("chybí");
        expect(toks[3].charSpan.start).toBe(15);
        expect(toks[3].charSpan.end).toBe(20);
        expect(toks[3].origText).toBe("chybý");
        expect(toks[3].origCharSpan.start).toBe(15);
        expect(toks[3].origCharSpan.end).toBe(20);
        expect(toks[4].text).toBe("nové");
        expect(toks[4].charSpan.start).toBe(21);
        expect(toks[4].charSpan.end).toBe(25);
        expect(toks[4].origText).toBe("nové");
        expect(toks[4].origCharSpan.start).toBe(21);
        expect(toks[4].origCharSpan.end).toBe(25);
        expect(toks[5].text).toBe("alba");
        expect(toks[5].charSpan.start).toBe(26);
        expect(toks[5].charSpan.end).toBe(30);
        expect(toks[5].origText).toBe("albumy");
        expect(toks[5].origCharSpan.start).toBe(26);
        expect(toks[5].origCharSpan.end).toBe(32);
        expect(toks[6].text).toBe("🤣");
        expect(toks[6].charSpan.start).toBe(31);
        expect(toks[6].charSpan.end).toBe(33);
        expect(toks[6].origText).toBe("🤣");
        expect(toks[6].origCharSpan.start).toBe(33);
        expect(toks[6].origCharSpan.end).toBe(35);
        expect(toks[7].text).toBe(".");
        expect(toks[7].charSpan.start).toBe(33);
        expect(toks[7].charSpan.end).toBe(34);
        expect(toks[7].origText).toBe(".");
        expect(toks[7].origCharSpan.start).toBe(35);
        expect(toks[7].origCharSpan.end).toBe(36);
    };

    const withOrig = examples.exampleAnalysisV320();
    let analysis = readFromJson(withOrig);
    _test(analysis.paragraphs[0].sentences[0].tokens);

    const withCorr = examples.exampleAnalysisV311();
    analysis = readFromJson(withCorr);
    _test(analysis.paragraphs[0].sentences[0].tokens);
});

test("Codepoint offsets.", () => {
    const _test = (text: string, toks: Token[]) => {
        expect(toks[1].text).toBe("🐦🎾");
        expect(toks[1].charSpan.start).toBe(2);
        expect(toks[1].charSpan.end).toBe(6);
        expect(toks[1].charSpan.extractText(text)).toBe("🐦🎾");
        expect(toks[2].text).toBe("obchodě");
        expect(toks[2].charSpan.start).toBe(7);
        expect(toks[2].charSpan.end).toBe(14);
        expect(toks[2].charSpan.extractText(text)).toBe("obchodě");
        expect(toks[3].text).toBe("chybí");
        expect(toks[3].charSpan.start).toBe(15);
        expect(toks[3].charSpan.end).toBe(20);
        expect(toks[3].charSpan.extractText(text)).toBe("chybí");
        expect(toks[4].text).toBe("nové");
        expect(toks[4].charSpan.start).toBe(21);
        expect(toks[4].charSpan.end).toBe(25);
        expect(toks[4].charSpan.extractText(text)).toBe("nové");
        expect(toks[5].text).toBe("alba");
        expect(toks[5].charSpan.start).toBe(26);
        expect(toks[5].charSpan.end).toBe(30);
        expect(toks[5].charSpan.extractText(text)).toBe("alba");
        expect(toks[6].text).toBe("🤣");
        expect(toks[6].charSpan.start).toBe(31);
        expect(toks[6].charSpan.end).toBe(33);
        expect(toks[6].charSpan.extractText(text)).toBe("🤣");
        expect(toks[7].text).toBe(".");
        expect(toks[7].charSpan.start).toBe(33);
        expect(toks[7].charSpan.end).toBe(34);
        expect(toks[7].charSpan.extractText(text)).toBe(".");
    };

    const withCodepoint = examples.exampleAnalysisV321();
    let analysis = readFromJson(withCodepoint);
    _test(
        analysis.paragraphs[0].text,
        analysis.paragraphs[0].sentences[0].tokens,
    );

    const notCodepoint = examples.exampleAnalysisV320();
    analysis = readFromJson(notCodepoint);
    _test(
        analysis.paragraphs[0].text,
        analysis.paragraphs[0].sentences[0].tokens,
    );
});

test("Reading sentiment from JSON.", () => {
    const analysis = readFromJson(examples.exampleAnalysisArticle());
    const sentiment = analysis.sentiment as Sentiment;

    expect(sentiment.label).toBe("neutral");
    expect(sentiment.mean).toBe(-0.1);
    expect(sentiment.negative).toBe(-0.1);
    expect(sentiment.positive).toBe(0.0);

    expect(analysis.paragraphs[2].sentences[0].sentiment!.label).toBe(
        "negative",
    );
    expect(analysis.paragraphs[2].sentences[0].sentiment!.mean).toBe(-0.2);
    expect(analysis.paragraphs[2].sentences[0].sentiment!.negative).toBe(-0.2);
});

test("Reading used chars value from JSON.", () => {
    const analysis = readFromJson(examples.exampleAnalysisArticle());

    expect(analysis.usedChars).toBe(2595);
});

test("SDK-41 Empty relation arguments.", () => {
    const analysis = readFromJson(examples.exampleAnalysisBugFixSDK41());
    const relation = analysis.relations[1];

    expect(relation.args.length).toBe(0);
});

test("SDK-44 Token fnc, subFnc and fullFnc parsing and serialization.", () => {
    const json = examples.exampleAnalysisBugFixSDK44();
    const analysis = readFromJson(json);
    const toks = Array.from(analysis.tokens());

    expect(toks[0].fnc).toBe(UDep.NMOD);
    expect(toks[0].subFnc).toBe("poss");
    expect(toks[0].fullFnc).toBe("nmod:poss");

    expect(toks[1].fnc).toBe(UDep.NSUBJ);
    expect(toks[1].subFnc).toBe("pass");
    expect(toks[1].fullFnc).toBe("nsubj:pass");

    expect(toks[2].fnc).toBe(UDep.AUX);
    expect(toks[2].subFnc).toBeNull();
    expect(toks[2].fullFnc).toBe("aux");

    expect(writeToJson(analysis)).toEqual(json);
});

test("SDK-45 Metadata parsing.", () => {
    const json = examples.exampleAnalysisBugFixSDK45();
    const analysis = readFromJson(json);
    const metadata = analysis.metadata;

    expect(metadata?.get("primitive")).toBe(100);
    expect(metadata?.get("array")).toEqual([3, "abc", true]);
    expect(metadata?.get("object")).toEqual({ a: 100, b: 200 });

    expect(writeToJson(analysis)).toEqual(json);
});

test("SDK-46 Support empty relation.args.", () => {
    const json = examples.exampleAnalysisBugFixSDK46();
    const analysis = readFromJson(json);
    const relations = analysis.relations;

    expect(relations[0].args.length).toBe(1);
    expect(relations[1].args.length).toBe(1);
    expect(relations[2].args.length).toBe(0);

    expect(writeToJson(analysis)).toEqual(json);
});

test("Tags with properties.", () => {
    const json = examples.exampleTagsWithProps();
    const analysis = readFromJson(json);

    expect(analysis.tags[0].gkbProperties).toEqual([]);
    expect(analysis.tags[1].gkbProperties).toEqual([
        new GkbProperty(
            "description",
            "popis",
            null,
            null,
            null,
            null,
            "německá politička",
        ),
        new GkbProperty(
            "region",
            "Kraj",
            "G1588",
            null,
            null,
            null,
            "Louisiana",
        ),
        new GkbProperty("country", "Stát", "G30", null, null, null, "USA"),
        new GkbProperty(
            "population",
            "Počet obyvatel",
            null,
            null,
            null,
            383997,
        ),
    ]);

    expect(writeToJson(analysis)).toEqual(json);
});

test("Entities with properties.", () => {
    const json = examples.exampleEntitiesWithProps();
    const analysis = readFromJson(json);

    expect(analysis.entities[0].gkbProperties).toEqual([]);
    expect(analysis.entities[1].gkbProperties).toEqual([
        new GkbProperty(
            "description",
            "popis",
            null,
            null,
            null,
            null,
            "město v USA",
        ),
        new GkbProperty(
            "region",
            "Kraj",
            "G1588",
            null,
            null,
            null,
            "Louisiana",
        ),
        new GkbProperty("country", "Stát", "G30", null, null, null, "USA"),
        new GkbProperty(
            "population",
            "Počet obyvatel",
            null,
            null,
            null,
            383997,
        ),
    ]);

    expect(writeToJson(analysis)).toEqual(json);
});
