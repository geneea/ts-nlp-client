import { exampleRequest } from "../examples";
import {
    AnalysisType,
    Diacritization,
    LanguageCode,
    Request,
    RequestBuilder,
    TextType,
} from "../../../src/g3/client/request";

const reqJson = exampleRequest();

test("Simple request build.", () => {
    let req = new RequestBuilder().build({ text: "Unit Test" });

    expect(req.toJson()).toEqual({ text: "Unit Test" });

    req = new RequestBuilder().build({ title: "", text: "" });

    expect(req.toJson()).toEqual({ title: "", text: "" });
});

test("More advanced request build.", () => {
    const builder = new RequestBuilder();

    builder.analyses = [AnalysisType.ENTITIES, AnalysisType.SENTIMENT];
    builder.domain = "news";
    builder.language = LanguageCode.EN;
    builder.textType = TextType.CLEAN;
    builder.diacritization = Diacritization.YES;
    builder.returnMentions = true;
    builder.returnItemSentiment = true;
    builder.customConfig = new Map([["custom_key", ["custom value"]]]);

    const builtReq = builder.build({
        title: "Angela Merkel in New Orleans",
        text: "Angela Merkel left Germany. She move to New Orleans to learn jazz. That's amazing.",
    });

    expect(builtReq.toJson()).toEqual(reqJson);
});

test("Read and write a request and compare to original.", () => {
    const req = Request.fromJson(reqJson);
    const output = req.toJson();

    expect(output).toEqual(reqJson);
});

test("Reference dates.", () => {
    const builder = new RequestBuilder();

    builder.referenceDate = "2019-05-12";
    expect(builder.referenceDate).toBe("2019-05-12");

    builder.referenceDate = "2019-5-2";
    expect(builder.referenceDate).toBe("2019-05-02");

    builder.referenceDate = "NOW";
    expect(builder.referenceDate).toBe("NOW");

    expect(() => {
        builder.referenceDate = "not a valid date";
    }).toThrow();

    expect(() => {
        builder.referenceDate = "2019-15-02";
    }).toThrow();
});
