/* eslint-disable @typescript-eslint/no-explicit-any */
import { HasId } from "../../common/id";
import { Cp2JSOffsetConverter } from "../../common/offset-converter";
import { SemVer } from "../../common/sem-ver";
import { CharSpan } from "../../common/char-span";
import { UDep, UPosFromStr, UDepFromStr, UDepToStr } from "../";
import {
    Analysis,
    Entity,
    EntityMention,
    Language,
    Node,
    Paragraph,
    Relation,
    RelationArgument,
    RelationSupport,
    Sentence,
    Sentiment,
    Tag,
    TagMention,
    TectoToken,
    Token,
    Tree,
    TreeBuilder,
    Vector,
} from "../model";
import { GkbProperty } from "../model/gkb-propert";

/**
 * Reads the G3 object from a JSON object as returned from Geneea G3 API.
 * @param raw Raw JSON object corresponding to the G3 API.
 * @returns G3 object encapsulating the analysis.
 *
 * Note: depending on the requested set of analyses and language support many of the keys can be missing.
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function readFromJson(raw: any): Analysis {
    return new Reader().fromJson(raw);
}

/** Standard keys used in G3 analysis JSON. */
const STD_KEYS = new Set([
    "id",
    "language",
    "paragraphs",
    "entities",
    "tags",
    "relations",
    "docSentiment",
    "itemSentiments",
    "docVectors",
    "itemVectors",
    "usedChars",
    "metadata",
    "debugInfo",
    "version",
]);

const RE_FULL_DEP_FNC = /^([a-z]+):([a-z]+)$/i;

class Reader {
    // Note: this implementation is not thread-safe.

    /** Registry to store read objects by their ids; used to resolve references. */
    private readonly registry = new Map<string, HasId>();

    private readonly id2sentiment = new Map<string, Sentiment>();
    private readonly id2vectors = new Map<string, Vector[]>();

    private readonly mentionId2tectoToken = new Map<string, TectoToken[]>();
    private readonly entityId2derivedMentions = new Map<
        string,
        EntityMention[]
    >();

    private offMap: OffsetMapping;

    /** Version of the JSON being read. */
    private version = new SemVer(3, 3, 0);

    register(obj: HasId) {
        this.registry.set(obj.id, obj);
    }

    /**
     * Resolves a string id into the associated object.
     * @param id String id to resolve.
     * @returns The object associated with the id.
     * @throws Throws an error when the id does not map to any object.
     */
    resolveId<T extends HasId>(id: string): T {
        const obj = this.registry.get(id);
        if (obj === undefined) {
            throw new Error(`Unknown object ID used as a reference ${id}.`);
        }
        return obj as T;
    }

    resolveIds<T extends HasId>(ids: string[]): T[] {
        const objs = ids
            .map((id) => this.resolveId(id))
            .filter((o) => o !== null);
        return objs as T[];
    }

    readAndResolveId<T extends HasId>(raw: any, key: string): T | null {
        return raw[key] ? this.resolveId<T>(raw[key]) : null;
    }

    readAndResolveIds<T extends HasId>(raw: any, key: string): T[] {
        return this.resolveIds(raw[key]);
    }

    checkVersion(version: string | null): void {
        const semVer = SemVer.valueOf(version ?? "3.0.0"); // 3.0.0 was not specified
        if (semVer.major > 3) {
            throw new Error(
                `Unsupported API version ${version}, major ver.num > 3.`,
            );
        } else if (semVer.isSameOrNewerThan(new SemVer(3, 4, 0))) {
            console.log(
                `Reading analysis with version ${version} higher than ${this.version} is only partially supported.`,
            );
        }
        this.version = semVer;
    }

    readSentiment(raw: any): Sentiment {
        return new Sentiment(raw.mean, raw.label, raw.positive, raw.negative);
    }

    readVector(raw: any): Vector {
        return new Vector(raw.name, raw.version, raw.values);
    }

    readVectors(raw: any): Vector[] {
        return (raw as any[]).map((v) => this.readVector(v));
    }

    readFeats(raw: any | null): Map<string, string> {
        return new Map<string, string>(Object.entries(raw));
    }

    readGkbProperty(raw: any): GkbProperty {
        return new GkbProperty(
            raw.name,
            raw.label,
            raw.valueGkbId,
            raw.boolValue,
            raw.floatValue,
            raw.intValue,
            raw.strValue,
        );
    }

    readToken(raw: any, tokenIdx: number, useOrigTextField: boolean): Token {
        let text: string;
        let origText: string;
        let off: number;
        let origOff: number;

        if (useOrigTextField) {
            text = raw.text;
            off = this.offMap.get(raw.off);
            origText = raw.origText ?? text;
            origOff = raw.origOff ? this.offMap.getOrig(raw.origOff) : off;
        } else {
            text = raw.corrText;
            off = this.offMap.get(raw.corrOff);
            origText = raw.text;
            origOff = this.offMap.getOrig(raw.off);
        }

        const pos = raw.pos ? UPosFromStr(raw.pos) : null;

        const fncStr: string | null | undefined = raw.fnc;
        let fnc = null;
        let subFnc = null;

        if (fncStr) {
            if (fncStr.toUpperCase() === "CLAUSE") {
                fnc = UDep.ROOT;
                subFnc = null;
            } else {
                const matches = fncStr.match(RE_FULL_DEP_FNC);
                if (matches !== null) {
                    fnc = UDepFromStr(matches[1]);
                    subFnc = matches[2];
                } else {
                    fnc = UDepFromStr(fncStr);
                    subFnc = null;
                }
            }
        }

        const tok = Token.of(
            raw.id,
            tokenIdx, // sentence based index
            text,
            CharSpan.withLen(off, text.length),
            origText,
            CharSpan.withLen(origOff, origText.length),
            raw.dLemma,
            raw.lemma,
            pos,
            raw.mTag,
            raw.feats ? this.readFeats(raw.feats) : null,
            fnc,
            subFnc,
        );
        this.register(tok);
        return tok;
    }

    readTectoToken(raw: any, tokenIdx: number): TectoToken {
        const tt = TectoToken.of(
            raw.id,
            tokenIdx,
            raw.fnc ?? UDepToStr(UDep.DEP),
            raw.lemma,
            raw.feats ? this.readFeats(raw.feats) : null,
            this.readAndResolveIds(raw, "tokenIds"),
            null, // EntityMention will be filled later
            null, // Entity will be filled later
        );

        if (raw.entityMentionId) {
            let tts: TectoToken[] = [];

            if (this.mentionId2tectoToken.has(raw.entityMentionId))
                tts = this.mentionId2tectoToken.get(
                    raw.entityMentionId,
                ) as TectoToken[];

            tts.push(tt);
            this.mentionId2tectoToken.set(raw.entityMentionId, tts);
        }

        this.register(tt);
        return tt;
    }

    createTree<T extends Node<T>>(
        rawTokens: any[],
        tokens: T[],
    ): Tree<T> | null {
        const tb = new TreeBuilder<T>().addNodes(tokens);
        for (const rt of rawTokens) {
            if (rt.parId) {
                const parent: T = this.resolveId(rt.parId);
                const child: T = this.resolveId(rt.id);
                tb.addDependency(child.idx, parent.idx);
            }
        }
        return tb.build();
    }

    readSentence(raw: any, useOrigTextField: boolean): Sentence {
        const rawTokens: any[] = raw.tokens;
        const tokens = rawTokens.map((raw, idx) =>
            this.readToken(raw, idx, useOrigTextField),
        );

        const rawTectoTokens: any[] = raw.tecto ?? [];
        const tectoTokens = rawTectoTokens.map((raw, idx) =>
            this.readTectoToken(raw, idx),
        );

        let tree: Tree<Token> | null;
        let tectoTree: Tree<TectoToken> | null;
        if (tokens[0].fnc != null) {
            tree = this.createTree(rawTokens, tokens);
            tectoTree = this.createTree(rawTectoTokens, tectoTokens);
        } else {
            tree = null;
            tectoTree = null;
        }

        const sent = Sentence.of(
            raw.id,
            tree?.root ?? null,
            tokens,
            tectoTree?.root,
            tectoTree?.tokens,
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
        );
        sent.tokens.forEach((t) => (t.sentence = sent));
        sent.tectoTokens?.forEach((tt) => (tt.sentence = sent));
        this.register(sent);
        return sent;
    }

    readParagraph(raw: any): Paragraph {
        const useOrigTextField = this.version.isSameOrNewerThan(
            new SemVer(3, 2, 0),
        );
        const hasCodepointOffs = this.version.isSameOrNewerThan(
            new SemVer(3, 2, 1),
        );

        const text = useOrigTextField ? raw.text : raw.corrText;
        const origText = useOrigTextField ? raw.origText : raw.text;
        this.offMap = hasCodepointOffs
            ? new Cp2JSOffsetMapping(text, origText ?? null)
            : new IdentityOffsetMapping();

        const para = Paragraph.of(
            raw.id,
            raw.type,
            text,
            origText,
            (raw.sentences as any[]).map((s) =>
                this.readSentence(s, useOrigTextField),
            ),
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
        );
        para.sentences.forEach((s) => (s.paragraph = para));
        this.register(para);
        return para;
    }

    readEntityMention(raw: any): EntityMention {
        const men = EntityMention.of(
            raw.id,
            raw.text,
            raw.mwl,
            this.readAndResolveIds<Token>(raw, "tokenIds"),
            raw.feats ? this.readFeats(raw.feats) : null,
            null, // derivedFrom; will be filled later
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
        );

        const entityId = raw.derivedFromEntityId;
        if (entityId) {
            const derivedMentions = this.entityId2derivedMentions.get(entityId);
            if (derivedMentions) derivedMentions.push(men);
            else this.entityId2derivedMentions.set(entityId, [men]);
        }

        this.register(men);
        return men;
    }

    readEntity(raw: any): Entity {
        const hasGkbProps =
            this.version.isSameOrNewerThan(new SemVer(3, 3, 0)) &&
            Array.isArray(raw.gkbProperties);
        const ent = Entity.of(
            raw.id,
            raw.gkbId,
            raw.stdForm,
            raw.type,
            raw.mentions
                ? (raw.mentions as any[]).map((m) => this.readEntityMention(m))
                : null,
            raw.feats ? this.readFeats(raw.feats) : null,
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
            hasGkbProps
                ? raw.gkbProperties.map((p: any) => this.readGkbProperty(p))
                : [],
        );
        ent.mentions.forEach((m) => (m.mentionOf = ent));
        this.register(ent);
        return ent;
    }

    readTagMention(raw: any): TagMention {
        const men = TagMention.of(
            raw.id,
            this.readAndResolveIds<Token>(raw, "tokenIds"),
            raw.feats ? this.readFeats(raw.feats) : null,
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
        );
        this.register(men);
        return men;
    }

    readTag(raw: any): Tag {
        const hasGkbProps =
            this.version.isSameOrNewerThan(new SemVer(3, 3, 0)) &&
            Array.isArray(raw.gkbProperties);
        const tag = Tag.of(
            raw.id,
            raw.gkbId,
            raw.stdForm,
            raw.type,
            raw.relevance,
            raw.mentions
                ? (raw.mentions as any[]).map((m) => this.readTagMention(m))
                : null,
            raw.feats ? this.readFeats(raw.feats) : null,
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
            hasGkbProps
                ? raw.gkbProperties.map((p: any) => this.readGkbProperty(p))
                : [],
        );
        tag.mentions.forEach((m) => (m.mentionOf = tag));
        this.register(tag);
        return tag;
    }

    readRelationArgument(raw: any): RelationArgument {
        return new RelationArgument(
            raw.name,
            raw.type,
            this.readAndResolveId<Entity>(raw, "entityId"),
        );
    }

    readRelationSupport(raw: any): RelationSupport {
        return RelationSupport.of(
            this.readAndResolveIds<Token>(raw, "tokenIds"),
            this.readAndResolveId<TectoToken>(raw, "tectoId"),
        );
    }

    readRelation(raw: any): Relation {
        const rel = Relation.of(
            raw.id,
            raw.textRepr,
            raw.name,
            raw.type,
            (raw.args ?? []).map((obj: any) => this.readRelationArgument(obj)),
            raw.support
                ? (raw.support as any[]).map((obj) =>
                      this.readRelationSupport(obj),
                  )
                : null,
            raw.feats ? this.readFeats(raw.feats) : null,
            this.id2sentiment.get(raw.id) ?? null,
            this.id2vectors.get(raw.id) ?? null,
        );
        this.register(rel);
        return rel;
    }

    readMetadata(raw: any): Map<string, any | null> | null {
        const useTopLevelMetadata = this.version.isOlderThan(
            new SemVer(3, 1, 0),
        );

        let metadata = raw.metadata
            ? new Map<string, any | null>(Object.entries(raw.metadata))
            : null;
        const unknownKeys: string[] = (Object.keys(raw) as string[]).filter(
            (k) => !STD_KEYS.has(k),
        );
        if (unknownKeys.length !== 0) {
            if (useTopLevelMetadata && metadata === null) {
                metadata = new Map<string, any | null>();
                unknownKeys.forEach((k) => metadata?.set(k, raw[k]));
            } else {
                console.warn(
                    `[Warning] unrecognized fields in the analysis: ${unknownKeys}`,
                );
            }
        }
        return metadata;
    }

    /**
     * Reads the Analysis object from a JSON object as returned from Geneea G3 API.
     * @param rawAnalysis Object corresponding to a G3 API JSON.
     * @returns An Analysis object encapsulating the NLP analysis.
     */
    fromJson(rawAnalysis: any): Analysis {
        const ra = rawAnalysis;

        this.checkVersion(ra.version);

        // store item sentiment to fill it in when the relevant items are constructed
        if (ra.itemSentiments) {
            const itemSentiments = Object.entries(ra.itemSentiments) as [
                string,
                any,
            ][];
            itemSentiments.map(([id, obj]) =>
                this.id2sentiment.set(id, this.readSentiment(obj)),
            );
        }

        // store item vectors to fill them in when the relevant items are constructed
        if (ra.itemVectors) {
            const itemVectors = Object.entries(ra.itemVectors) as [
                string,
                any,
            ][];
            itemVectors.map(([id, obj]) => {
                const vectors = obj as any[];
                this.id2vectors.set(
                    id,
                    vectors.map((v) => this.readVector(v)),
                );
            });
        }

        const docId = ra.id ?? null;

        // ISO 639-2 for Undetermined language.
        const language = new Language(ra.language?.detected ?? "und");

        const paragraphs = ((ra.paragraphs || []) as any[]).map((raw) =>
            this.readParagraph(raw),
        );
        const entities = ((ra.entities || []) as any[]).map((raw) =>
            this.readEntity(raw),
        );
        const tags = ((ra.tags || []) as any[]).map((raw) => this.readTag(raw));
        const relations = ((ra.relations || []) as any[]).map((raw) =>
            this.readRelation(raw),
        );

        const docSentiment = ra.docSentiment
            ? this.readSentiment(ra.docSentiment)
            : null;
        const docVectors = ra.docVectors
            ? this.readVectors(ra.docVectors)
            : null;

        const usedChars = ra.usedChars ?? null;
        const metadata = this.readMetadata(ra);
        const debugInfo = ra.debugInfo ?? null;

        const analysis = new Analysis(
            docId,
            language,
            paragraphs,
            docSentiment,
            entities,
            tags,
            relations,
            docVectors,
            usedChars,
            metadata,
            debugInfo,
        );

        analysis.paragraphs.forEach((x) => (x.container = analysis));

        // fill derived-from entities for mentions
        this.entityId2derivedMentions.forEach((mentions, id) => {
            const entity = this.resolveId<Entity>(id);
            mentions.forEach((m) => (m.derivedFrom = entity));
        });

        // fill tecto-token entity mention
        this.mentionId2tectoToken.forEach((tokens, id) => {
            const mention = this.resolveId<EntityMention>(id);
            tokens.forEach((tt) => {
                tt.entityMention = mention;
                tt.entity = mention.mentionOf;
            });
        });

        return analysis;
    }
}

interface OffsetMapping {
    get(cpOff: number): number;
    getOrig(cpOff: number): number;
}

/** Encapsulates mapping of offsets from code-points to JavaScript string indices. */
class Cp2JSOffsetMapping implements OffsetMapping {
    get: (cpOff: number) => number;
    getOrig: (cpOff: number) => number;

    constructor(text: string, origText: string | null) {
        const convForText = new Cp2JSOffsetConverter(text);
        const convForOrigText =
            origText === null
                ? convForText
                : new Cp2JSOffsetConverter(origText);

        this.get = (cpOff) => convForText.convert(cpOff);
        this.getOrig = (cpOff) => convForOrigText.convert(cpOff);
    }
}

class IdentityOffsetMapping implements OffsetMapping {
    get(cpOff: number): number {
        return cpOff;
    }

    getOrig(cpOff: number): number {
        return cpOff;
    }
}
