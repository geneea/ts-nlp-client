import { objToStr } from "../../common/common";
import { HasId } from "../../common/id";

/** Interface of items that can be assigned a list of vectors. */
interface HasVectors extends HasId {
    vectors: Vector[] | null;
}

/** Class encapsulating a vector. */
class Vector {
    /**
     *
     * @param name Name identifying the model of this vector.
     * @param version A particular version of the model which produced this vector.
     * @param values The vector values.
     */
    constructor(
        public name: string,
        public version: string,
        public values: number[],
    ) {}

    /** Dimension of this vector. */
    get dimension(): number {
        return this.values.length;
    }

    toString(): string {
        return objToStr(this, Object.keys(this));
    }
}

export { HasVectors, Vector };
