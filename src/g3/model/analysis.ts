import { objToStr } from "../../common/common";
import { Entity } from "./entity";
import { Language } from "./language";
import { Paragraph } from "./paragraph";
import { Relation } from "./relation";
import { Sentence } from "./sentence";
import { HasSentiment, Sentiment } from "./sentiment";
import { Tag } from "./tag";
import { TectoToken } from "./tecto-token";
import { Token } from "./token";
import { HasVectors, Vector } from "./vector";

/** An object encapsulating the results of NLP analysis. */
class Analysis implements HasSentiment, HasVectors {
    /**
     * Creates an Analysis object.
     * @param docId Document id.
     * @param language Language of the document and analysis.
     * @param paragraphs The paragraphs within the document. For F2, these are segments.
     * @param docSentiment Sentiment of the document.
     * @param entities The entities in the document.
     * @param tags The tags of the document.
     * @param relations The relations in the document.
     * @param docVectors Optional vectors for the whole document.
     * @param usedChars Characters billed for the analysis.
     * @param metadata The extra non-NLP type of information related to analysis.
     * @param debugInfo Debugging information, if any.
     */
    constructor(
        public docId: string | null = null,
        public language: Language,
        public paragraphs: Paragraph[],
        public docSentiment: Sentiment | null = null,
        public entities: Entity[],
        public tags: Tag[],
        public relations: Relation[],
        public docVectors: Vector[] | null = null,
        public usedChars: number | null = null,
        public metadata: Map<string, unknown> | null = null,
        public debugInfo: unknown = null,
    ) {}

    get id(): string {
        return this.docId ?? "";
    }

    get sentiment(): Sentiment | null {
        return this.docSentiment;
    }

    get vectors(): Vector[] | null {
        return this.docVectors;
    }

    /** Sentences across all paragraphs. */
    *sentences(): Iterable<Sentence> {
        for (const p of this.paragraphs) {
            for (const s of p.sentences) yield s;
        }
    }

    /** Tokens across all paragraphs. */
    *tokens(): Iterable<Token> {
        for (const p of this.paragraphs) {
            for (const t of p.tokens()) yield t;
        }
    }

    /** Tecto tokens across all paragraphs. */
    *tectoTokens(): Iterable<TectoToken> {
        for (const p of this.paragraphs) {
            for (const tt of p.tectoTokens()) yield tt;
        }
    }

    /** Returns the top [n] tags sorted by `relevance` in descending order. */
    topTags(n = -1): Tag[] {
        const topTags = this.tags.sort((a, b) => b.relevance - a.relevance);
        return n === -1 ? topTags : topTags.slice(0, n);
    }

    /**
     * Returns a paragraph with the specified type.
     * Throws IllegalArgumentException if there are more than one such paragraphs, and return null if there are none.
     * This is intended for legacy paragraphs corresponding to title/lead/text segments.
     *
     * For standard types, use these predefined constants:
     * [Paragraph.TYPE_TITLE], [Paragraph.TYPE_ABSTRACT], [Paragraph.TYPE_BODY], [Paragraph.TYPE_SECTION_HEADING],
     * or dedicated methods: [title], [lead], [body].
     */
    getParaByType(paraType: string): Paragraph | null {
        const paras = this.paragraphs.filter((p) => p.type === paraType);
        if (paras.length > 1) {
            throw new Error(`Multiple paragraphs with the type ${paraType}`);
        }
        return paras.length === 1 ? paras[0] : null;
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple title paragraphs.
     */
    titlePara(): Paragraph | null {
        return this.getParaByType(Paragraph.TYPE_TITLE);
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple subject paragraphs.
     */
    subjectPara(): Paragraph | null {
        return this.titlePara();
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple abstract paragraphs.
     */
    abstractPara(): Paragraph | null {
        return this.getParaByType(Paragraph.TYPE_ABSTRACT);
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple lead paragraphs.
     */
    leadPara(): Paragraph | null {
        return this.abstractPara();
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple perex paragraphs.
     */
    perexPara(): Paragraph | null {
        return this.abstractPara();
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple body paragraphs.
     */
    bodyPara(): Paragraph | null {
        return this.getParaByType(Paragraph.TYPE_BODY);
    }

    /**
     * Returns the title paragraph if present, null if not, and throws
     * an error if there are multiple text paragraphs.
     */
    textPara(): Paragraph | null {
        return this.bodyPara();
    }

    toString(): string {
        return objToStr(this, [
            "docId",
            "language",
            "paragraphs",
            "docSentiment",
            "entities",
            "tags",
            "relations",
            "docVectors",
            "usedChars",
            "metadata",
            "debugInfo",
        ]);
    }
}

export { Analysis };
