import { CharSpan } from "../../common/char-span";
import { objToStr } from "../../common/common";
import { HasId } from "../../common/id";
import { Paragraph } from "./paragraph";
import { HasSentiment, Sentiment } from "./sentiment";
import { TectoToken } from "./tecto-token";
import { Token } from "./token";
import { HasVectors, Vector } from "./vector";

/** A single sentence with its morphological, syntactical, deep-syntactical and sentimental analysis. */
class Sentence implements HasId, HasSentiment, HasVectors {
    /** Type of a paragraph representing a title of the whole document. Also used for email subjects. */
    static readonly TYPE_TITLE = "TITLE";
    /** Type of a paragraph representing an abstract (lead or perex) of the whole document. */
    static readonly TYPE_ABSTRACT = "ABSTRACT";
    /** Type of a paragraph containing regular text (for now this is used for the whole body of the document). */
    static readonly TYPE_BODY = "BODY";
    /** Type of a paragraph representing a section/chapter heading (not used yet). */
    static readonly TYPE_SECTION_HEADING = "section_heading";

    /** The paragraph containing this sentence. */
    /** TODO: osetrit ze je inicializovany */
    paragraph: Paragraph;

    /**
     *
     * @param id ID of the sentence used to refer to it from other objects.
     * @param root Token which is the root of the syntactic structure of the sentence.
     * @param tokens All tokens of the sentence ordered by word-order.
     * @param tectoRoot Tecto token which is the root of the tecto structure of the sentence.
     * @param tectoTokens All tecto tokens of the sentence; the order has no meaning; null if not avaialable.
     * @param sentiment Optional sentiment of the sentence.
     * @param vectors Optional vectors of this paragraph.
     */
    private constructor(
        readonly id: string,
        public root: Token | null,
        public tokens: Token[],
        public tectoRoot: TectoToken | null,
        public tectoTokens: TectoToken[] | null,
        public sentiment: Sentiment | null,
        public vectors: Vector[] | null,
    ) {}

    /** Sentence factory method, public constructor. */
    static of(
        id: string,
        root: Token | null,
        tokens: Token[],
        tectoRoot: TectoToken | null = null,
        tectoTokens: TectoToken[] | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
    ): Sentence {
        return new Sentence(
            id,
            root,
            tokens,
            tectoRoot,
            tectoTokens,
            sentiment,
            vectors,
        );
    }

    /** Text span within the paragraph. */
    charSpan(): CharSpan {
        return CharSpan.of(
            this.tokens[0].charSpan.start,
            this.tokens[this.tokens.length - 1].charSpan.end,
        );
    }

    /** Text of the sentence, possibly after correction. */
    text(): string {
        return this.charSpan().extractText(this.paragraph.text);
    }

    /** Text span within the original paragraph. */
    origCharSpan(): CharSpan {
        return CharSpan.of(
            this.tokens[0].origCharSpan.start,
            this.tokens[this.tokens.length - 1].origCharSpan.end,
        );
    }

    /** Text of the sentence in the original paragraph. */
    origText(): string {
        return this.origCharSpan().extractText(this.paragraph.origText);
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "tokens",
            "root",
            "tectoRoot",
            "tectoTokens",
            "sentiment",
            "vectors",
        ]);
    }
}

export { Sentence };
