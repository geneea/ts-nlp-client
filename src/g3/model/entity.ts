import { objToStr } from "../../common/common";
import { HasId } from "../../common/id";
import { GkbProperty, HasGkbProperties } from "./gkb-propert";
import { Sentence } from "./sentence";
import { HasSentiment, Sentiment } from "./sentiment";
import { TokenSupport, Token } from "./token";
import { HasVectors, Vector } from "./vector";

/** A data class encapsulating an Entity. */
class Entity implements HasId, HasSentiment, HasVectors, HasGkbProperties {
    /**
     *
     * @param id ID of the entity used to refer to it from other objects.
     * @param gkbId Unique identifier of this entity in Geneea knowladge base.
     * @param stdForm Standard form of the entity, abstracting from alternative names.
     * @param type Basic type of this entity (e.g. person, location, ...).
     * @param mentions Actual occurences of this entity in the text. Empty if not requested/supported.
     * @param feats Custom features/properties.
     * @param sentiment Sentiment of this entity. null if not requested.
     * @param vectors Optional vectors for this entity.
     * @param gkbProperties GKB properties. Since 3.3.0.
     */
    private constructor(
        readonly id: string,
        public gkbId: string | null = null,
        public stdForm: string,
        public type: string,
        public mentions: EntityMention[],
        public feats: Map<string, string>,
        public sentiment: Sentiment | null = null,
        public vectors: Vector[] | null = null,
        public gkbProperties: GkbProperty[],
    ) {}

    /** Entity factory method, public constructor. */
    static of(
        id: string,
        gkbId: string | null = null,
        stdForm: string,
        type: string,
        mentions: EntityMention[] | null = null,
        feats: Map<string, string> | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
        gkbProperties: GkbProperty[] | null = null,
    ): Entity {
        return new Entity(
            id,
            gkbId,
            stdForm,
            type,
            mentions ?? [],
            feats ?? new Map<string, string>(),
            sentiment,
            vectors,
            gkbProperties ?? [],
        );
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "gkbId",
            "stdForm",
            "type",
            "mentions",
            "feats",
            "sentiment",
            "vectors",
            "gkbProperties",
        ]);
    }
}

/** A single occurrence of an entity in the text. */
class EntityMention implements HasId, HasSentiment, HasVectors {
    /** Entity this mention belongs to. Set from outside.  */
    mentionOf: Entity;

    /**
     *
     * @param id ID of the mention used to refer to it from other objects.
     * @param text The form of this entity mention, as it occurs in the text.
     * @param mwl Lemma of this mention (potentially multiword lemma), i.e. base form of the entity expression.
     * @param tokens Tokens of this entity mention.
     * @param feats Custom features/properties.
     * @param derivedFrom Entity from which this mention can be derived (e.g. mention `salmon` for entity `fish`), if applicable.
     * @param sentiment Sentiment of this mention. Note: Not supported yet.
     * @param vectors Optional vectors for this mention.
     */
    private constructor(
        readonly id: string,
        public text: string,
        public mwl: string,
        public tokens: TokenSupport,
        public feats: Map<string, string>,
        public derivedFrom: Entity | null,
        public sentiment: Sentiment | null,
        public vectors: Vector[] | null,
    ) {}

    /** EntityMention factory method, public constructor. */
    static of(
        id: string,
        text: string,
        mwl: string,
        tokens: Token[],
        feats: Map<string, string> | null = null,
        derivedFrom: Entity | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
    ): EntityMention {
        return new EntityMention(
            id,
            text,
            mwl,
            TokenSupport.of(tokens),
            feats ?? new Map<string, string>(),
            derivedFrom,
            sentiment,
            vectors,
        );
    }

    /**
     * Sentence containing this entity mention.
     * Entity mention belongs to maximally one sentence; artificial mentions without tokens belong to no sentence.
     */
    sentence(): Sentence {
        return this.tokens.sentence;
    }

    /** Checks whether the entity mention is continuous (most are).  */
    isContinuous(): boolean {
        return this.tokens.isContinuous;
    }

    /** True iff this entity mention is derived from some other entity (e.g. mention `salmon` for entity `fish`). */
    isDerived(): boolean {
        return this.derivedFrom != null;
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "text",
            "mwl",
            "tokens",
            "feats",
            "derivedFrom",
            "sentiment",
            "vectors",
        ]);
    }
}

export { Entity, EntityMention };
