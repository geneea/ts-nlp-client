import { objToStr } from "../../common/common";
import { HasId } from "../../common/id";

/** Interface of items that can be assigned a sentiment value. */
interface HasSentiment extends HasId {
    sentiment: Sentiment | null;
}

/** Class encapsulating sentiment of a document, sentence or relation. */
class Sentiment {
    /**
     *
     * @param mean Average sentiment.
     * @param label Human readable label describing the average sentiment.
     * @param positive Average sentiment of positive items.
     * @param negative Average sentiment of negative items.
     */
    constructor(
        public mean: number,
        public label: string,
        public positive: number,
        public negative: number,
    ) {}

    toString(): string {
        return objToStr(this, Object.keys(this));
    }
}

export { HasSentiment, Sentiment };
