import { objToStr } from "../../common/common";

/** Language of the document. */
class Language {
    /** Language of the document as detected. */
    constructor(public detected: string) {}

    toString(): string {
        return objToStr(this, Object.keys(this));
    }
}

export { Language };
