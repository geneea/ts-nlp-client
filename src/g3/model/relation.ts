import { objToStr } from "../../common/common";
import { HasId } from "../../common/id";
import { Entity } from "./entity";
import { HasSentiment, Sentiment } from "./sentiment";
import { TectoToken } from "./tecto-token";
import { TokenSupport, Token } from "./token";
import { HasVectors, Vector } from "./vector";

/** An object encapsulating a relation extracted from text. For example `buy(John, car)`. */
class Relation implements HasId, HasSentiment, HasVectors {
    /** Attribute relation (e.g. `good(pizza)` for _good pizza_, _pizza is good_). */
    static readonly TYPE_ATTR = "attr";
    /** Verbal relation (e.g. `eat(pizza)` for _eat a pizza_. */
    static readonly TYPE_RELATION = "relation";
    /**
     * Relation where at least one argument is outside of the the document (e.g. between `pizza` in the document and
     * `food` item in the knowledgebase).
     */
    static readonly TYPE_EXTERNAL = "external";
    /** Key presence signifies it is a negated word, value = True.*/
    static readonly FEAT_NEGATED = "negated";
    /** Feature storing info about modality. */
    static readonly FEAT_MODALITY = "modality";

    /**
     *
     * @param id ID of the relation used to refer to it from other objects.
     * @param textRepr Human redable representation of the relation, e.g. `eat-not(SUBJ:John, DOBJ:pizza)`.
     * @param name Name of the relation, e.g. `eat` for _eat a pizza_ or `good` for _a good pizza_.
     * @param type One of Relation.TYPE_ATTR, Relation.TYPE_RELATION, Relation.TYPE_EXTERNAL.
     * @param args Arguments of the relation (subject, possibly an object).
     * @param support Tecto-tokens of all the mentions of the relations (restricted to its head). Empty if not requested.
     * @param feats Any features of the relation e.g. modality: can.
     * @param sentiment Sentiment of this relation. null if not requested.
     * @param vectors Optional vectors for this relation.
     */
    private constructor(
        readonly id: string,
        public textRepr: string,
        public name: string,
        public type: string,
        public args: RelationArgument[],
        public support: RelationSupport[],
        public feats: Map<string, string>,
        public sentiment: Sentiment | null,
        public vectors: Vector[] | null,
    ) {}

    /** Relation factory method, public constructor. */
    static of(
        id: string,
        textRepr: string,
        name: string,
        type: string,
        args: RelationArgument[],
        support: RelationSupport[] | null = null,
        feats: Map<string, string> | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
    ): Relation {
        return new Relation(
            id,
            textRepr,
            name,
            type,
            args,
            support ?? [],
            feats ?? new Map<string, string>(),
            sentiment,
            vectors,
        );
    }

    isNegated(): boolean {
        return this.feats.get(Relation.FEAT_NEGATED) === "true" ? true : false;
    }

    modality(): string | null {
        return this.feats.get(Relation.FEAT_MODALITY) ?? null;
    }

    /** Returns the relation subject argument, if precisely one is present; otherwise returns null. */
    subj(): RelationArgument | null {
        const args: RelationArgument[] = this.args.filter(
            (x) => x.type.toUpperCase() === "SUBJECT",
        );
        return args.length === 1 ? args[0] : null;
    }

    /** Returns the relation object argument, if precisely one is present; otherwise returns null. */
    obj(): RelationArgument | null {
        const args: RelationArgument[] = this.args.filter(
            (x) => x.type.toUpperCase() === "OBJECT",
        );
        return args.length === 1 ? args[0] : null;
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "textRepr",
            "name",
            "type",
            "args",
            "feats",
            "support",
            "sentiment",
            "vectors",
        ]);
    }
}

/** Object representing an argument (subject/object) of a relation. */
class RelationArgument {
    /**
     *
     * @param name Name of the argument (e.g. John).
     * @param type Type of the argument (subject, object).
     * @param entity The entity corresponding to this argument, if any. null if the argument is not an entity.
     */
    constructor(
        public name: string,
        public type: string,
        public entity: Entity | null,
    ) {}

    toString(): string {
        return objToStr(this, ["name", "type"]);
    }
}

/** Tokens corresponding to a single head (predicate) of a relation. */
class RelationSupport {
    /**
     *
     * @param tokens Tokens corresponding to the head of the relation.
     * @param tectoToken Tecto token corresponding to the tokens. null if tecto tokens are not part of the model.
     */
    private constructor(
        public tokens: TokenSupport,
        public tectoToken: TectoToken | null,
    ) {}

    /** RelationSupport factory method, public constructor. */
    static of(
        tokens: Token[],
        tectoToken: TectoToken | null = null,
    ): RelationSupport {
        return new RelationSupport(TokenSupport.of(tokens), tectoToken);
    }

    toString(): string {
        return objToStr(this, Object.keys(this));
    }
}

export { Relation, RelationArgument, RelationSupport };
