import { objToStr } from "../../common/common";
import { Entity, EntityMention } from "./entity";
import { Node, NodeUtils } from "./node";
import { Token, TokenSupport } from "./token";

/**
 * A tecto token, i.e. a tectogrammatical abstraction of a word (e.g. "did not sleep" are three tokens but a single
 * tecto-token)
 * Tecto tokens have an zero-based index reflecting their position within their sentence.
 */
class TectoToken extends Node<TectoToken> {
    private constructor(
        readonly id: string,
        readonly idx: number,
        /** Label of the dependency edge. */
        public fnc: string,
        /** Tecto lemma. */
        public lemma: string,
        /** Grammatical and other features of the tecto token.  */
        public feats: Map<string, string>,
        /**
         * Surface token corresponding to this tecto token; not necessarily adjacent; ordered by word-order;
         * Might be null, because the root or dropped phrases have no surface realization.
         */
        public tokens: TokenSupport | null,
        /** Entity mention associated with this tecto token; null if there is no such entity.  */
        public entityMention: EntityMention | null,
        /**
         * Legacy value: Entity associated with this tecto token; null if there is no such entity.
         * (Used only by F3, not by G3)
         */
        public entity: Entity | null,
    ) {
        super();
    }

    /** TectoToken factory method, public constructor. */
    static of(
        id: string,
        idx: number,
        fnc: string,
        lemma: string,
        feats: Map<string, string> | null = null,
        tokens: Token[] | null = null,
        entityMention: EntityMention | null = null,
        entity: Entity | null = null,
    ): TectoToken {
        let tokenSupport: TokenSupport | null = null;
        if (tokens !== null && tokens.length > 0) {
            tokenSupport = new TokenSupport(
                NodeUtils.sorted(tokens) as Token[],
                NodeUtils.isContinuous(tokens),
            );
        }

        return new TectoToken(
            id,
            idx,
            fnc,
            lemma,
            feats ?? new Map<string, string>(),
            tokenSupport,
            entityMention,
            entity,
        );
    }

    /** Converts the tecto token to a default non-recursive string: index + lemma.  */
    toSimpleString(): string {
        return this.toStringWith(true, false);
    }

    /** Converts the token to a non-recursive string: index + [lemma] + [fnc]. */
    toStringWith(lemma: boolean, fnc: boolean): string {
        const l = lemma ? `:${this.lemma}` : "";
        const f = fnc ? `:${this.fnc}` : "";
        return `${this.idx}${l}${f}`;
    }

    toString(): string {
        return objToStr(this, ["id", "idx", "fnc", "lemma", "feats"]);
    }
}

export { TectoToken };
