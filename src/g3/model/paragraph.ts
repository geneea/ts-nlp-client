import { Analysis } from "./analysis";
import { Token } from "./token";
import { Sentence } from "./sentence";
import { HasSentiment, Sentiment } from "./sentiment";
import { HasVectors, Vector } from "./vector";
import { TectoToken } from "./tecto-token";
import { HasId } from "../../common/id";
import { objToStr } from "../../common/common";

export class Paragraph implements HasId, HasSentiment, HasVectors {
    /** Type of a paragraph representing a title of the whole document. Also used for email subjects.  */
    static readonly TYPE_TITLE = "TITLE";
    /** Type of a paragraph representing an abstract (lead or perex) of the whole document. */
    static readonly TYPE_ABSTRACT = "ABSTRACT";
    /** Type of a paragraph containing regular text (for now this is used for the whole body of the document). */
    static readonly TYPE_BODY = "BODY";
    /** Type of a paragraph representing a section/chapter heading (not used yet). */
    static readonly TYPE_SECTION_HEADING = "section_heading";

    /** The full analysis object containing this paragraph. */
    container: Analysis;

    /**
     *
     * @param id ID of the paragraph used to refer to it from other objects.
     * @param type Title, section heading, lead, body text, etc. For now, it is simply the segment type: title, lead, body.
     * @param text The paragraph text, possibly after correction (token offsets link here).
     * @param origText The original paragraph text as in the request.
     * @param sentences The sentences the paragraph consists of.
     * @param sentiment Optional sentiment of the paragraph.
     * @param vectors Optional vectors for this paragraph.
     */
    private constructor(
        readonly id: string,
        public type: string,
        public text: string,
        public origText: string,
        public sentences: Sentence[],
        public sentiment: Sentiment | null,
        public vectors: Vector[] | null,
    ) {}

    /** Paragraph factory method, public constructor. */
    static of(
        id: string,
        type: string,
        text: string,
        origText: string | null = null,
        sentences: Sentence[] | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
    ): Paragraph {
        return new Paragraph(
            id,
            type,
            text,
            origText === null || text === origText ? text : origText,
            sentences ?? [],
            sentiment,
            vectors,
        );
    }

    /** Tokens across all sentences. */
    *tokens(): Iterable<Token> {
        for (const s of this.sentences) {
            for (const t of s.tokens) yield t;
        }
    }

    /** Tecto tokens across all sentences. */
    *tectoTokens(): Iterable<TectoToken> {
        for (const s of this.sentences) {
            if (s.tectoTokens !== null) {
                for (const tt of s.tectoTokens) yield tt;
            }
        }
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "type",
            "text",
            "origText",
            "sentences",
            "sentiment",
            "vectors",
        ]);
    }
}
