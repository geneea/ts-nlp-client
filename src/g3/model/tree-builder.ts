import { isSequential } from "./../../common/common";
import { Node, NodeUtils } from "./node";

/** A temporary class used during the construction of a tree of tokens or tecto-tokens.  */
class Tree<T extends Node<T>> {
    constructor(
        readonly root: T,
        readonly tokens: T[],
    ) {}
}

class TreeBuilder<T extends Node<T>> {
    private readonly nodes: T[] = [];
    /** Dependency edges between nodes. */
    private readonly deps: Map<number, number> = new Map();

    /**
     * Record a single token as a node of the tree.
     * @param node Token to add. Its index must be correct, parent and children fields are ignored.
     * @returns The builder to allow chained calls.
     */
    addNode(node: T): TreeBuilder<T> {
        this.nodes.push(node);
        return this;
    }

    /**
     * Record a collection of tokens as nodes of the tree.
     * @param nodes Tokens to add. Their index must be correct, parent and children fields are ignored.
     * @returns The builder to allow chained calls.
     */
    addNodes(nodes: Iterable<T>): TreeBuilder<T> {
        for (const n of nodes) this.nodes.push(n);
        return this;
    }

    /**
     * Record a dependency edge. The tokens connected by the edge might be added later.
     * @param childIdx Index of the child token (note: tokens are indexed within their sentence).
     * @param parentIdx Index of the parent token (note: tokens are indexed within their sentence);
     * @returns The builder to allow chained calls.
     */
    addDependency(childIdx: number, parentIdx: number): TreeBuilder<T> {
        if (childIdx < 0) throw new Error(`Negative node index ${childIdx}.`);
        if (parentIdx < 0) throw new Error(`Negative node index ${parentIdx}`);
        if (childIdx === parentIdx)
            throw new Error("Dependency edge cannot be reflexive.");
        if (this.deps.has(childIdx) && this.deps.get(childIdx) !== parentIdx)
            throw new Error(
                `Node ${childIdx} has multiple parents: [${
                    this.nodes[parentIdx].id
                }, ${this.nodes[this.deps.get(childIdx)!].id}].`,
            );

        this.deps.set(childIdx, parentIdx);
        return this;
    }

    /** All nodes are hanged to the first one. */
    addDummyDependencies(): void {
        if (this.deps.size !== 0)
            throw new Error(
                "Dummy dependencies cannot be added when other dependencies have been specified.",
            );

        if (this.nodes.length === 0) return;

        NodeUtils.sort(this.nodes);
        for (const n of this.nodes.slice(1)) this.addDependency(n.idx, 0);
    }

    private fillParents(): void {
        const maxIdx = this.nodes.length - 1;

        for (const [c, p] of this.deps) {
            if (c > maxIdx)
                throw new Error(
                    `The child of the dependency edge ${c} -> ${p} is out of range (max=${maxIdx}).`,
                );
            if (p > maxIdx)
                throw new Error(
                    `The parent of the dependency edge ${c} -> ${p} is out of range (max=${maxIdx}).`,
                );

            this.nodes[c].parent = this.nodes[p];
        }
    }

    private findRoot(): T {
        const roots = this.nodes.filter((n) => n.isRoot());
        if (roots.length === 0) throw new Error("No root.");
        if (roots.length > 1)
            throw new Error(`Multiple roots: ${roots.map((r) => r.id)}.`);

        return roots[0];
    }

    private fillChildren(): void {
        for (const n of this.nodes) {
            n.children = this.nodes.filter((x) => x.parent === n);
        }
    }

    /** Builds a tree based on the current state. Should not be called more than once. */
    build(): Tree<T> | null {
        if (this.nodes.length === 0) return null;

        // Creates an ordered dependency tree based on the contents this builder.
        NodeUtils.sort(this.nodes);
        if (
            this.nodes[0].idx !== 0 ||
            !isSequential(this.nodes.map((x) => x.idx))
        ) {
            throw new Error("Indexes are not sequential.");
        }

        this.fillParents();
        const root = this.findRoot(); // exactly one root check; addDependency checks for multiple parents => tree
        this.fillChildren();

        return new Tree(root, this.nodes);
    }
}

export { Tree, TreeBuilder };
