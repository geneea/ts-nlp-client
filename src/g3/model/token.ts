import { CharSpan } from "../../common/char-span";
import { objToStr } from "../../common/common";
import { UDep, UDepToStr } from "../../common/udep";
import { UPos } from "../../common/upos";
import { Node, NodeUtils } from "./node";
import { Paragraph } from "./paragraph";
import { Sentence } from "./sentence";

/**
 * A token including basic morphological and syntactic information.
 * A token is similar to a word, but includes punctuation.
 * Tokens have an zero-based index reflecting their position within their sentence.
 * The morphological and syntactical features might be null (deepLemma, lemma, morphTag, pos, fnc, parent),
 * or empty (children) if not requested or supported.
 */
class Token extends Node<Token> {
    /** Lemma info features, a list of strings.  */
    static readonly FEAT_LEMMA_INFO = "lemmaInfo";
    /** Key presence signifies it is a negated word, value = true. */
    static readonly FEAT_NEGATED = "negated";
    /** Key presence signifies it is an unknown word, value = true. */
    static readonly FEAT_UNKNOWN = "unknown";

    private constructor(
        readonly id: string,
        readonly idx: number,
        /** Text of this token, possibly after correction. */
        public text: string,
        /** Cahracter span within the paragraph. */
        public charSpan: CharSpan,
        /** Text of this token in the original paragraph. */
        public origText: string,
        /** Character span within the original paragraph. */
        public origCharSpan: CharSpan,
        /** Lemma of the token e.g. bezpecny. null if not requested/supported. */
        public deepLemma: string | null,
        /** Simple lemma of the token, e.g. nejnebezpecnejsi (in Cz, includes negation and grade). Null if not requested/supported. */
        public lemma: string | null,
        /** Google universal tag. Null if not requested/supported.  */
        public pos: UPos | null,
        /** Morphological tag, e.g. AAMS1-...., VBD, ... Null if not requested/supported.  */
        public morphTag: string | null,
        /** Universal and custom features  */
        public feats: Map<string, string>,
        /** Label of the dependency edge. Null if not requested/supported. */
        public fnc: UDep | null,
        /** Dependency sub-function. None if not requested/supported. */
        public subFnc: string | null,
    ) {
        super();
    }

    /** Token factory method, public constructor. */
    static of(
        id: string,
        idx: number,
        text: string,
        charSpan: CharSpan,
        origText: string | null = null,
        origCharSpan: CharSpan | null = null,
        deepLemma: string | null = null,
        lemma: string | null = null,
        pos: UPos | null = null,
        morphTag: string | null = null,
        feats: Map<string, string> | null = null,
        fnc: UDep | null = null,
        subFnc: string | null = null,
    ): Token {
        return new Token(
            id,
            idx,
            text,
            charSpan,
            origText === null || text === origText ? text : origText,
            origCharSpan === null || charSpan === origCharSpan
                ? charSpan
                : origCharSpan,
            deepLemma,
            lemma,
            pos,
            morphTag,
            feats ?? new Map<string, string>(),
            fnc,
            subFnc,
        );
    }

    /** True iff the token form contains a negation prefix. */
    isNegated(): boolean {
        return this.feats.has(Token.FEAT_NEGATED);
    }

    /** True iff the token is unknown to the lemmatizer. The lemma provided is the same as the token itself. */
    isUnknown(): boolean {
        return this.feats.has(Token.FEAT_UNKNOWN);
    }

    /**
     * Token following of preceding this token within the sentence.
     * @param offset Relative offset. The following tokens have a positive offse,
     *    preceding a negative one. The ext token has offset = 1.
     * @returns The token at the relative offset or null if the offset is invalid.
     */
    offsetToken(offset: number): Token | null {
        const i = this.idx + offset;
        const tokens = this.sentence.tokens;
        return 0 <= i && i < tokens.length ? tokens[i] : null;
    }

    /** The previous token or null if this token is sentence initial.  */
    previous(): Token | null {
        return this.offsetToken(-1);
    }

    /** The next token or null if this token is sentence final. */
    next(): Token | null {
        return this.offsetToken(1);
    }

    /**
     * Full dependency function in the format `{fnc}:{subFnc}` if the sub-function is present.
     * Otherwise it's the same as `fnc`.
     */
    get fullFnc(): string | null {
        if (this.fnc !== null) {
            const fnc = UDepToStr(this.fnc);
            return this.subFnc !== null ? `${fnc}:${this.subFnc}` : fnc;
        } else return null;
    }

    /** Converts the token to a default non-recursive string: index + text.  */
    toSimpleString(): string {
        return this.toStringWith(true, false, false);
    }

    /** Converts the token to a non-recursive string: index + [text] + [pos] + [fnc]. */
    toStringWith(text: boolean, pos: boolean, fnc: boolean): string {
        const t = text ? `:${this.text}` : "";
        const p = pos ? `:${this.pos ?? "_"}` : "";
        const f = fnc ? `:${this.fnc ?? "_"}` : "";
        return `${this.idx}${t}${p}${f}`;
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "idx",
            "text",
            "charSpan",
            "origText",
            "origCharSpan",
            "deepLemma",
            "lemma",
            "pos",
            "feats",
            "morphTag",
            "fnc",
            "subFnc",
        ]);
    }
}

/**
 * Tokens within a single sentence; ordered by word-order; non-empty, continuous or discontinuous.
 */
class TokenSupport {
    /**
     *
     * @param tokens The tokens of this support.
     * @param isContinuous Is this support a continuous sequence of tokens, i.e. a token span?
     */
    constructor(
        public tokens: Token[],
        public isContinuous: boolean,
    ) {}

    /**
     * Creates a TokenSupport object from a list of tokens.
     * @param tokens Non-empty list of tokens (no need for them to be sorted).
     */
    static of(tokens: Token[]): TokenSupport {
        if (tokens.length === 0) {
            throw new Error("TokenSupport cannot be empty.");
        }
        if (!NodeUtils.isFromSameSentence(tokens)) {
            throw new Error("Tokens are not from the same sentence.");
        }
        return new TokenSupport(
            NodeUtils.sorted(tokens) as Token[],
            NodeUtils.isContinuous(tokens),
        );
    }

    get sentence(): Sentence {
        return this.tokens[0].sentence;
    }

    get paragraph(): Paragraph {
        return this.tokens[0].sentence.paragraph;
    }

    get first(): Token {
        return this.tokens[0];
    }

    get last(): Token {
        return this.tokens[this.tokens.length - 1];
    }

    get size(): number {
        return this.tokens.length;
    }

    get ids(): string[] {
        return this.tokens.map((t) => t.id);
    }

    /**
     * The character span between the first and last token relative to the enclosing paragraph;
     * for discontinuous support this includes intervening gaps.
     */
    get charSpan(): CharSpan {
        return CharSpan.of(this.firstCharParaOffset, this.lastCharParaOffset);
    }

    /** Offset of the first character of these tokens  within the enclosing paragraph.  */
    get firstCharParaOffset(): number {
        return this.first.charSpan.start;
    }

    /** Offset of the last character of these tokens within the enclosing paragraph.  */
    get lastCharParaOffset(): number {
        return this.last.charSpan.end;
    }

    /**
     * Substring of a full text as denoted by this support (before correction).
     * For discontinuous supports, the result includes the intervening gaps.
     */
    get text(): string {
        return this.charSpan.extractText(this.sentence.paragraph.text);
    }

    /** Breaks this token support into continuous sub-sequences of tokens. */
    spans(): TokenSupport[] {
        if (this.isContinuous) {
            return [this];
        } else {
            const spans: TokenSupport[] = [];
            let start = 0;
            let prev = this.tokens[0];

            for (let i = 1; i < this.tokens.length; i++) {
                if (prev.idx + 1 !== this.tokens[i].idx) {
                    spans.push(
                        new TokenSupport(this.tokens.slice(start, i), true),
                    );
                    start = i;
                }
                prev = this.tokens[i];
            }

            spans.push(new TokenSupport(this.tokens.slice(start), true));
            return spans;
        }
    }

    /** The coverage texts of each of the continuous spans, ordered by word-order. */
    textSpans(): string[] {
        return this.spans().map((s) => s.text);
    }

    toString(): string {
        return "[" + this.tokens.map((t) => t.toString()).join(", ") + "]";
    }
}

export { Token, TokenSupport };
