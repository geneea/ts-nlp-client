/** Interface of elements that can be assigned a list of properties. Entities and tags. */
interface HasGkbProperties {
    gkbProperties: GkbProperty[];
}

/** Class encapsulating a vector */
class GkbProperty {
    /**
     *
     * @param name Key identifying the property.
     * @param label Label describing the property.
     * @param valueGkbId Identifier of GKB item representing property value if there is any.
     * @param boolValue Property value if it is boolean.
     * @param floatValue Property value if it is float.
     * @param intValue Property value if it is integer.
     * @param strValue Property value if it is string or date.
     */
    constructor(
        readonly name: string,
        readonly label: string | null = null,
        readonly valueGkbId: string | null = null,
        readonly boolValue: boolean | null = null,
        readonly floatValue: number | null = null,
        readonly intValue: number | null = null,
        readonly strValue: string | null = null,
    ) {
        if (
            boolValue === null &&
            floatValue === null &&
            intValue === null &&
            strValue === null
        ) {
            throw Error("No GKB property value was set.");
        }
    }
}

export { HasGkbProperties, GkbProperty };
