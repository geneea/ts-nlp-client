import { objToStr } from "../../common/common";
import { HasId } from "../../common/id";
import { GkbProperty, HasGkbProperties } from "./gkb-propert";
import { Sentence } from "./sentence";
import { HasSentiment, Sentiment } from "./sentiment";
import { TokenSupport, Token } from "./token";
import { HasVectors, Vector } from "./vector";

class Tag implements HasId, HasSentiment, HasVectors, HasGkbProperties {
    /** Type of the tag with the main topic of the document  */
    static readonly TYPE_TOPIC: string = "topic";
    /** Type of the tags with the topic distribution of the document  */
    static readonly TYPE_TOPIC_DISTRIBUTION = "topic.distribution";

    /**
     *
     * @param id ID of the tag used to refer to it from other objects.
     * @param gkbId Unique identifier of this tag in Geneea knowledge-base. null if not found.
     * @param stdForm Standard form of the tag, abstracting from its alternative names.
     * @param type Domain-specific type (e.g. content, theme, iAB, department).
     * @param relevance Relevance of the tag relative to the content of the document.
     * @param mentions Text segments related to this tag. Empty if not appropriate/requested/supported.
     * @param feats Custom features.
     * @param sentiment Sentiment of this tag. Not supported yet.
     * @param vectors Optional vectors for this tag.
     * @param gkbProperties GKB properties. Since 3.3.0.
     */
    private constructor(
        readonly id: string,
        public gkbId: string | null,
        public stdForm: string,
        public type: string,
        public relevance: number,
        public mentions: TagMention[],
        public feats: Map<string, string>,
        public sentiment: Sentiment | null,
        public vectors: Vector[] | null,
        public gkbProperties: GkbProperty[],
    ) {}

    /** Tag factory method, public constructor. */
    static of(
        id: string,
        gkbId: string | null,
        stdForm: string,
        type: string,
        relevance: number,
        mentions: TagMention[] | null = null,
        feats: Map<string, string> | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
        gkbProperties: GkbProperty[] | null = null,
    ): Tag {
        return new Tag(
            id,
            gkbId,
            stdForm,
            type,
            relevance,
            mentions ?? [],
            feats ?? new Map<string, string>(),
            sentiment,
            vectors,
            gkbProperties ?? [],
        );
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "gkbId",
            "stdForm",
            "type",
            "relevance",
            "feats",
            "mentions",
            "sentiment",
            "vectors",
            "gkbProperties",
        ]);
    }
}

/** A single occurence of a tag in the text. */
class TagMention implements HasSentiment, HasVectors {
    /** Tag this mention belongs to. Set from outsied.  */
    mentionOf: Tag;

    /**
     *
     * @param id ID of the mention used to refer to it from other objects.
     * @param tokens Tokens of this tag mention.
     * @param feats Custom features/properties.
     * @param sentiment Sentiment of this mention. Not supported yet.
     * @param vectors Optional vectors for this tag mention.
     */
    private constructor(
        readonly id: string,
        public tokens: TokenSupport,
        public feats: Map<string, string>,
        public sentiment: Sentiment | null,
        public vectors: Vector[] | null,
    ) {}

    /** TagMention factory method, public constructor. */
    static of(
        id: string,
        tokens: Token[],
        feats: Map<string, string> | null = null,
        sentiment: Sentiment | null = null,
        vectors: Vector[] | null = null,
    ): TagMention {
        return new TagMention(
            id,
            TokenSupport.of(tokens),
            feats ?? new Map<string, string>(),
            sentiment,
            vectors,
        );
    }

    /**
     * Sentence containing this tag mention.
     * Tag mention belongs to maximally one sentence; artificial mentions without tokens belong to no sentence.
     */
    sentence(): Sentence {
        return this.tokens.sentence;
    }

    /** Checks whether the tag mention is continuous (most are).  */
    isContinuous(): boolean {
        return this.tokens.isContinuous;
    }

    toString(): string {
        return objToStr(this, [
            "id",
            "tokens",
            "feats",
            "sentiment",
            "vectors",
        ]);
    }
}

export { Tag, TagMention };
