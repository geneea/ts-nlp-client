import { Agent } from "http";

import axios from "axios";
import { AxiosRequestConfig } from "axios";

import { Analysis, readFromJson } from "..";
import { Request } from "./request";

/**
 * A client for the Geneea NLP REST (General API V3).
 */
export class Client {
    /** The default address of the Geneea NLP G3 API. */
    static readonly DEFAULT_URL = "https://api.geneea.com/v3/analysis";
    static readonly DEFAULT_TIMEOUT_MILLISEC = 600000;

    private httpAgent: Agent;

    constructor(
        readonly url: string,
        readonly userKey: string,
        readonly timeout: number,
    ) {
        this.httpAgent = new Agent({ keepAlive: true });
    }

    /**
     * Call Geneea G3 API.
     * @param request A Request instance (data to analyze and parameters).
     * @returns A promise that resolves to the result as an Analysis object.
     */
    async analyze(request: Request): Promise<Analysis> {
        const options: AxiosRequestConfig = {
            method: "post",
            headers: {
                Accept: "application/json; charset=UTF-8",
                Authorization: `user_key ${this.userKey}`,
                "Content-Type": "application/json; charset=UTF-8",
            },
            timeout: this.timeout,
            responseType: "json",
            httpAgent: this.httpAgent,
        };

        try {
            const response = await axios.post(
                this.url,
                request.toJson(),
                options,
            );
            return readFromJson(response.data);
        } catch (error) {
            if (error.response) {
                throw new Error(
                    `API call failure with HTTP code ${error.response.status}
                        ${JSON.stringify(error.response.data)}`,
                );
            } else {
                throw error;
            }
        }
    }

    /**
     *  Create Geneea G3 Client.
     *
     *  @param url URL of the Interpretor API to use.
     *  @param userKey API user key; if not specified, loaded from `GENEEA_API_KEY` environment variable.
     *  @param timeout A number specifying the socket timeout in milliseconds. This will set the timeout before the socket is connected. [DEFAULT_TIMEOUT_SEC] if not set.
     *  @returns A G3 client.
     */
    static create(
        url = this.DEFAULT_URL,
        userKey: string | undefined = "ANONYMOUS",
        timeout = this.DEFAULT_TIMEOUT_MILLISEC,
    ): Client {
        const key = process.env.GENEEA_API_KEY ?? userKey;
        if (!key) {
            console.warn(
                "[Warning] No user key specified: neither as a parameter nor as a GENEEA_API_KEY environment variable",
            );
        }

        return new Client(url, key, timeout);
    }
}
