import { serializeMap } from "../../common/common";
import { Paragraph } from "../model";

/**
 * Object specifying the [text] and [type] of a single paragraph.
 */
export class ParaSpec {
    /**
     *
     * @param type Type of the paragraphs, typically one of [Paragraph.TYPE_TILE], [Paragraph.TYPE_ABSTRACT],
     * [Paragraph.TYPE_BODY]; possibly [Paragraph.TYPE_SECTION_HEADING]
     * @param text Text of the paragraph.
     */
    constructor(
        readonly type: string,
        readonly text: string,
    ) {}

    /** Paragraph representing a title of the whole document. It's equivalent to [subject]. */
    static title(text = ""): ParaSpec {
        return new ParaSpec(Paragraph.TYPE_TITLE, text ?? "");
    }

    /** Paragraph representing a subject of the document or email. It's equivalent to [title]. */
    static subject(text = ""): ParaSpec {
        return this.title(text);
    }

    /** Paragraph representing an abstract of the document. It's equivalent to [lead] and [perex]. */
    static abstract(text = ""): ParaSpec {
        return new ParaSpec(Paragraph.TYPE_ABSTRACT, text ?? "");
    }

    /** Paragraph representing a lead of the document. It's equivalent to [abstract] and [perex]. */
    static lead(text = ""): ParaSpec {
        return this.abstract(text);
    }

    /** Paragraph representing a perex of the document. It's equivalent to [abstract] and [lead].*/
    static perex(text = ""): ParaSpec {
        return this.abstract(text);
    }

    /** Paragraph containing a body of the document. It's equivalent to [text]. */
    static body(text = ""): ParaSpec {
        return new ParaSpec(Paragraph.TYPE_BODY, text ?? "");
    }

    /** Paragraph containing a text of the document. It's equivalent to [body]. */
    static text(text = ""): ParaSpec {
        return this.body(text);
    }
}

/**
 * The linguistic analyses the G3 API can perform;
 * [more detail][https://help.geneea.com/api_general/guide/analyses.html]
 */
export enum AnalysisType {
    /** Perform all analyses at once */
    ALL = "ALL",
    /** Recognize and standardize entities in text;
     * [more details][https://help.geneea.com/api_general/guide/entities.html]
     */
    ENTITIES = "ENTITIES",
    /** Assign semantic tags to a document.
     * [more details][<https://help.geneea.com/api_general/guide/tags.html]
     */
    TAGS = "TAGS",
    /** Relations between entities and their attributes;
     * [more details][<https://help.geneea.com/api_general/guide/relations.html]
     */
    RELATIONS = "RELATIONS",
    /** Detect the emotions of the author contained in the text;
     * [more details][https://help.geneea.com/api_general/guide/sentiment.html]
     */
    SENTIMENT = "SENTIMENT",
    /** Detect the language the text is written in;
     * [more details][https://help.geneea.com/api_general/guide/language.html]
     */
    LANGUAGE = "LANGUAGE",
}

export const parseStrToAnalysisType: (typeStr: string) => AnalysisType = (
    typeStr,
) => {
    const at = AnalysisType[typeStr.toUpperCase() as keyof typeof AnalysisType];
    if (!at) throw new Error(`Invalid analysis type '${typeStr}'`);
    return at;
};

/** Typically used ISO 639-1 language codes. */
export enum LanguageCode {
    CS = "cs",
    DE = "de",
    EN = "en",
    ES = "es",
    PL = "pl",
    SK = "sk",
}

/** Typically used domains. For more info [see][https://help.geneea.com/api_general/guide/domains.html] */
export enum Domain {
    /** General media articles. */
    MEDIA = "media",
    /** Media articles covering news. */
    NEWS = "news",
    /** Media articles covering sport news. */
    SPORT = "sport",
    /** Tabloid articles. */
    TABLOID = "tabloid",
    /** Media articles covering technology and science. */
    TECH = "tech",
    /** General Voice-of-the customer documents (e.g. reviews). */
    VOC = "voc",
    /** Voice-of-the customer documents covering banking (e.g. reviews of banks). */
    VOC_BANKING = "voc-banking",
    /** Voice-of-the customer documents covering restaurants (e.g. reviews of restaurants). */
    VOC_HOSPITALITY = "voc-hospitality",
}

/** Typically used text types.  */
export enum TextType {
    /** Text that is mostly grammatically, orthographically and typographically correct, e.g. news articles.  */
    CLEAN = "clean",
    /** Text that ignores many formal grammatical, orthographical and typographical conventions,
     * e.g. social media posts.
     */
    CASUAL = "casual",
}

/** Supported diacritization models. */
export enum Diacritization {
    /** No diacritization is performed. */
    NONE = "none",
    /** Diacritics is added if needed. */
    AUTO = "auto",
    /** Diacritics is added to words without it if needed. */
    YES = "yes",
    /** Diacritics is first removed and then added if needed. */
    REDO = "redo",
}

/** Standard keys used by the G3 request. */
const STD_KEYS = new Set([
    "id",
    "title",
    "text",
    "paraSpecs",
    "analyses",
    "htmlExtractor",
    "language",
    "langDetectPrior",
    "domain",
    "textType",
    "referenceDate",
    "diacritization",
    "returnMentions",
    "returnItemSentiment",
    "metadata",
]);

const DATE_FORMAT_REGEX =
    /^(\d{4})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/;

/**
 * An object encapsulating a single REST request for the G3 API.
 */
export class Request {
    readonly analyses: Set<AnalysisType> | null = null;

    /**
     *
     * @param id Unique identifier of the document.
     * @param title The title or subject of the document, when available; mutually exclusive with the ``paraSpecs`` parameter.
     * @param text The main text of the document; mutually exclusive with the ``paraSpecs`` parameter.
     * @param paraSpecs The document paragraphs; mutually exclusive with the `title` and `text` parameters.
     * @param analyses What analyses to return.
     * @param language The language of the document as ISO 639-1; auto-detection will be used if omitted.
     * @param langDetectPrior The language detection prior; e.g. ‘de,en’.
     * @param domain The source domain from which the document originates.
     * See the [available domains][https://help.geneea.com/api_general/guide/domains.html]
     * @param textType The type or genre of text; not supported in public workflows/domains yet.
     * @param referenceDate Date to be used for the analysis as a reference; values: “NOW” or in format YYYY-MM-DD.
     * @param diacritization Determines whether to perform text diacritization.
     * @param returnMentions Should entity/tag/relation mentions be returned? No mentions are returned if null.
     * @param returnItemSentiment Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null.
     * @param metadata Extra non-NLP type of information related to the document, key-value pairs.
     * @param customConfig
     */
    constructor(
        readonly id: string | null = null,
        readonly title: string | null = null,
        readonly text: string | null = null,
        readonly paraSpecs: ParaSpec[] | null = null,
        analyses: AnalysisType[] | null = null,
        readonly language: string | null = null,
        readonly langDetectPrior: string | null = null,
        readonly domain: string | null = null,
        readonly textType: string | null = null,
        readonly referenceDate: string | null = null,
        readonly diacritization: string | null = null,
        readonly returnMentions: boolean | null = null,
        readonly returnItemSentiment: boolean | null = null,
        readonly metadata: Map<string, string> | null = null,
        readonly customConfig: Map<string, unknown | null> | null = null,
    ) {
        this.analyses = analyses ? new Set(analyses) : null;
    }

    /** Converts a request instance to a JSON compatible object. */
    toJson(): unknown {
        const obj: Record<string, unknown> = {};

        if (this.id !== null) obj.id = this.id;
        if (this.title !== null) obj.title = this.title;
        if (this.text !== null) obj.text = this.text;
        if (this.language !== null) obj.language = this.language;
        if (this.langDetectPrior !== null)
            obj.langDetectPrior = this.langDetectPrior;
        if (this.domain !== null) obj.domain = this.domain;
        if (this.textType !== null) obj.textType = this.textType;
        if (this.referenceDate !== null) obj.referenceDate = this.referenceDate;
        if (this.diacritization !== null)
            obj.diacritization = this.diacritization;

        if (this.paraSpecs) {
            obj.paraSpecs = this.paraSpecs.map((ps) => {
                return { type: ps.type, text: ps.text };
            });
        }

        if (this.analyses) {
            obj.analyses = Array.from(this.analyses)
                .map((a) => a.toLowerCase())
                .sort();
        }

        if (this.returnMentions) obj.returnMentions = true;
        if (this.returnItemSentiment) obj.returnItemSentiment = true;

        if (this.metadata && this.metadata.size > 0)
            obj.metadata = serializeMap(this.metadata);

        if (this.customConfig) {
            for (const entry of this.customConfig.entries()) {
                obj[entry[0]] = entry[1];
            }
        }

        return obj;
    }

    /** Converts a request instance to a JSON string. */
    toJsonString(): string {
        return JSON.stringify(this.toJson());
    }

    /** Reads a request instance from a JSON object. */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    static fromJson(raw: any): Request {
        const title = raw.title ?? null;
        const text = raw.text ?? null;
        const paraSpecs = raw.paraSpecs ?? null;

        if ((text !== null || title !== null) && paraSpecs !== null)
            throw new Error(
                "Parameters text/title and paraSpecs are mutually exclusive.",
            );
        if (text === null && paraSpecs === null)
            throw new Error(
                "Either the text or paraSpecs parameter has to be provided",
            );

        const analyses = raw.analyses
            ? raw.analyses.map((a: string) => parseStrToAnalysisType(a))
            : null;

        let metadata = null;
        if (raw.metadata) {
            const entries: [string, string][] = Object.entries(
                raw.metadata,
            ).map((e: [string, string]) => [e[0], e[1].toString()]);
            metadata = new Map<string, string>(entries);
        }

        const unknownKeys = Object.keys(raw).filter((k) => !STD_KEYS.has(k));
        const customConfig =
            unknownKeys.length > 0
                ? new Map<string, string | null>(
                      unknownKeys.map((k) => [k, raw[k]]),
                  )
                : null;

        return new Request(
            raw.id ?? null,
            title,
            text,
            paraSpecs,
            analyses,
            raw.language ?? null,
            raw.lanDetectPrior ?? null,
            raw.domain ?? null,
            raw.textType ?? null,
            raw.referenceDate ?? null,
            raw.diacritization ?? null,
            raw.returnMentions ?? null,
            raw.returnItemSentiment ?? null,
            metadata,
            customConfig,
        );
    }
}

/**
 * An interface for objects to be passed to RequestBuilder's build() method.
 *
 * @member id Unique identifier of the document.
 * @member text The main text of the document; mutually exclusive with the ``paraSpecs`` parameter.
 * @member title The title or subject of the document, when available; mutually exclusive with the ``paraSpecs`` parameter.
 * @member paraSpecs The document paragraphs; mutually exclusive with `title` and `text` parameters.
 * @member language The language of the document as ISO 639-1; auto-detection will be used if null.
 * @member referenceDate Date to be used for the analysis as a reference; values: ``NOW`` or in format YYYY-MM-DD.
 *   No reference date is used if null.
 * @member metadata Extra non-NLP type of information related to the document, key-value pairs.
 * @member customConfig Any custom options passed to the G3 API endpoint.
 */
export interface RequestOptions {
    id?: string;
    text?: string;
    title?: string;
    paraSpecs?: ParaSpec[];
    language?: string;
    referenceDate?: string;
    metadata?: Map<string, string>;
    customConfig?: Map<string, unknown | null>;
}

/**
 * Creates a builder with fields meant to be shared across requests.
 *
 * When analyzing multiple documents, one should:
 *     * first, create a builder specifying all parameters shared by all the analyses to perform (e.g. [analyses] to perform,
 * [language prior][langDetectPrior]),
 *     * then, use the build function to create individual requests specifying the parameters that are specific
 * for the analysis of each document (Id and text of the document, but possibly also `language`, etc).
 */
export class RequestBuilder {
    private _analyses: Set<AnalysisType> | null = null;
    private _language: string | null = null;
    private _langDetectPrior: string | null = null;
    private _domain: string | null = null;
    private _textType: string | null = null;
    private _referenceDate: string | null = null;
    private _diacritization: string | null = null;
    private _returnMentions: boolean | null = null;
    private _returnItemSentiment: boolean | null = null;
    private _metadata: Map<string, string> | null = null;
    private _customConfig: Map<string, unknown | null> | null = null;

    /** What analyses to return  */
    get analyses(): AnalysisType[] | null {
        return this._analyses ? Array.from(this._analyses) : null;
    }

    /** What analyses to return  */
    set analyses(analyses: AnalysisType[] | null) {
        this._analyses = new Set(analyses);
    }

    /** The language of the document as ISO 639-1; auto-detection will be used if omitted.  */
    get language(): string | null {
        return this._language;
    }

    /** The language of the document as ISO 639-1; auto-detection will be used if omitted.  */
    set language(language: string | LanguageCode | null) {
        this._language = language;
    }

    /** The language detection prior; e.g. ‘de,en’.  */
    get langDetectPrior(): string | null {
        return this._langDetectPrior;
    }

    /** The language detection prior; e.g. ‘de,en’.  */
    set langDetectPrior(langDetectPrior: string | null) {
        this._langDetectPrior = langDetectPrior;
    }

    /** The source domain from which the document originates.
     * See the [available domains][https://help.geneea.com/api_general/guide/domains.html]
     */
    get domain(): string | null {
        return this._domain;
    }

    /** The source domain from which the document originates.
     * See the [available domains][https://help.geneea.com/api_general/guide/domains.html]
     */
    set domain(domain: string | Domain | null) {
        this._domain = domain;
    }

    /** The type or genre of text; not supported in public workflows/domains yet.  */
    get textType(): string | null {
        return this._textType;
    }

    /** The type or genre of text; not supported in public workflows/domains yet.  */
    set textType(textType: string | TextType | null) {
        this._textType = textType;
    }

    /** Date to be used for the analysis as a reference; either “NOW” or in format YYYY-MM-DD. */
    get referenceDate(): string | null {
        return this._referenceDate;
    }

    /** Date to be used for the analysis as a reference; either “NOW” or in format YYYY-MM-DD. */
    set referenceDate(referenceDate: string | null) {
        this._referenceDate = referenceDate
            ? this.formatRefDate(referenceDate)
            : null;
    }

    /** Determines whether to perform text diacritization  */
    get diacritization(): string | null {
        return this._diacritization;
    }

    /** Determines whether to perform text diacritization  */
    set diacritization(diacritization: string | Diacritization | null) {
        this._diacritization = diacritization;
    }

    /** Should entity/tag/relation mentions be returned? No mentions are returned if null.  */
    get returnMentions(): boolean | null {
        return this._returnMentions;
    }

    /** Should entity/tag/relation mentions be returned? No mentions are returned if null.  */
    set returnMentions(returnMentions: boolean | null) {
        this._returnMentions = returnMentions;
    }

    /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null.  */
    get returnItemSentiment(): boolean | null {
        return this._returnItemSentiment;
    }

    /** Should entity/mention/tag/relation etc. sentiment be returned? No sentiment is returned if null.  */
    set returnItemSentiment(returnItemSentiment: boolean | null) {
        this._returnItemSentiment = returnItemSentiment;
    }

    /** Extra non-NLP type of information related to the document, key-value pairs.  */
    get metadata(): Map<string, string> | null {
        return this._metadata;
    }

    /** Extra non-NLP type of information related to the document, key-value pairs.  */
    set metadata(metadata: Map<string, string> | null) {
        this._metadata = metadata;
    }

    /**
     * Add [custom options][customConfig] to the request builder to be passed to the G3 API endpoint.
     * Existing custom options are overwritten.
     */
    get customConfig(): Map<string, unknown | null> | null {
        return this._customConfig;
    }

    /**
     * Add [custom options][customConfig] to the request builder to be passed to the G3 API endpoint.
     * Existing custom options are overwritten.
     */
    set customConfig(customConfig: Map<string, unknown | null> | null) {
        if (customConfig !== null && containsStdKey(customConfig.keys())) {
            throw new Error(
                `Keys ${Array.from(
                    customConfig.keys(),
                ).sort()} overlap with the standard request keys`,
            );
        }
        if (this._customConfig === null) {
            this._customConfig = customConfig;
        } else if (customConfig !== null && customConfig.size > 0) {
            for (const entry of customConfig.entries()) {
                this._customConfig.set(entry[0], entry[1]);
            }
        }
    }

    private formatRefDate(date: string): string {
        if (date.toUpperCase() === "NOW") return "NOW";

        // eslint-disable-next-line prefer-const
        let { year, month, day } = this.parseRefDate(date);

        if (month.length < 2) month = "0" + month;
        if (day.length < 2) day = "0" + day;

        return [year, month, day].join("-");
    }

    private parseRefDate(date: string): ParsedDate {
        const matches = date.match(DATE_FORMAT_REGEX);
        if (matches === null)
            throw new Error(`Referene date ${date} is not valid.`);
        return { year: matches[1], month: matches[2], day: matches[3] };
    }

    /**
     * Creates a new request object to be passed to the G3 client.
     *
     * @param options A RequestOptions object.
     * @returns Request object to be passed to the G3 client.
     */
    build(options: RequestOptions): Request {
        const {
            id,
            text,
            title,
            paraSpecs,
            language,
            referenceDate,
            metadata,
            customConfig,
        } = options;
        if (
            (text !== undefined || title !== undefined) &&
            paraSpecs !== undefined
        ) {
            throw new Error(
                "Parameters text/title and paraSpecs are mutually exclusive",
            );
        }
        if (text === undefined && paraSpecs === undefined) {
            throw new Error(
                "Either the text or paraSpecs parameter has to be provided",
            );
        }
        if (customConfig !== undefined && containsStdKey(customConfig.keys())) {
            throw new Error(
                `Keys ${Array.from(
                    customConfig.keys(),
                ).sort()} overlap with the standard request keys`,
            );
        }

        const refDate = referenceDate
            ? this.formatRefDate(referenceDate)
            : this.referenceDate;

        return new Request(
            id,
            title,
            text,
            paraSpecs,
            this.analyses,
            language ?? this.language,
            this.langDetectPrior,
            this.domain,
            this.textType,
            refDate,
            this.diacritization,
            this.returnMentions,
            this.returnItemSentiment,
            combineMaps(this.metadata, metadata ?? null),
            combineMaps(this.customConfig, customConfig ?? null),
        );
    }
}

function containsStdKey(keys: Iterable<string>) {
    return Array.from(keys).filter((k) => STD_KEYS.has(k)).length > 0;
}

function combineMaps<K, V>(
    a: Map<K, V> | null,
    b: Map<K, V> | null,
): Map<K, V> | null {
    if (!a || a.size === 0) return b;
    if (!b || b.size === 0) return a;
    const c = new Map<K, V>();
    a.forEach((v, k) => c.set(k, v));
    b.forEach((v, k) => c.set(k, v));
    return c;
}

interface ParsedDate {
    year: string;
    month: string;
    day: string;
}
