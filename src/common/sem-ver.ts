export class SemVer {
    constructor(
        readonly major: number,
        readonly minor: number,
        readonly fix: number,
    ) {}

    static readonly REGEX = /^(\d+)\.(\d+).(\d+)$/;

    static valueOf(text: string): SemVer {
        const match = this.REGEX.exec(text);
        if (match === null) {
            throw new Error(`Semantic version requires ${this.REGEX.source}.`);
        }

        const [major, minor, fix] = match.splice(1).map((x) => Number(x));
        return new SemVer(major, minor, fix);
    }

    compareTo(other: SemVer): number {
        if (this.major != other.major) return this.major - other.major;
        if (this.minor != other.minor) return this.minor - other.minor;
        return this.fix - other.fix;
    }

    isOlderThan(other: SemVer): boolean {
        return this.compareTo(other) < 0;
    }

    isSameAs(other: SemVer): boolean {
        return this.compareTo(other) === 0;
    }

    isSameOrNewerThan(other: SemVer): boolean {
        return this.compareTo(other) >= 0;
    }
}
