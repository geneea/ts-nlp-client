/** Interface of items with an id value. */
interface HasId {
    id: string;
}

export { HasId };
