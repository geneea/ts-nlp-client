import { objToStr } from "./common";

/** Continuous non-empty span of text, relative to some large text. */
class CharSpan {
    /**
     *
     * @param start The first character of this span as a zero-based offset within the full text.
     * @param end Zero-based index of the character immediately following this span. The span cannot be empty.
     */
    constructor(
        public start: number,
        public end: number,
    ) {}

    /**
     * Creates a CharSpan object from [start] index and [end] index.
     * @param start The first character of this span as a zero-based offset within the full text.
     * @param end Index of the character immediately following this span.
     */
    static of(start: number, end: number): CharSpan {
        if (start < 0) {
            throw new RangeError(
                `Start character index cannot be negative (${start})`,
            );
        }
        if (start >= end) {
            throw new RangeError(
                `End character (${end}) must be after the start character (${start})`,
            );
        }
        return new CharSpan(start, end);
    }

    /**
     * Creates a CharSpan object from [start] index and text [length].
     * @param start The first character of this span as a zero-based offset within the full text.
     * @param length The length of this span.
     */
    static withLen(start: number, length: number): CharSpan {
        if (start < 0) {
            throw new RangeError(
                `Start character index cannot be negative (${start})`,
            );
        }
        if (length <= 0) {
            throw new RangeError(
                `Length has to be greater than zero (${length})`,
            );
        }
        return new CharSpan(start, start + length);
    }

    /** Length of the span in characters. */
    get length(): number {
        return this.end - this.start;
    }

    /** Substring of a full text as denoted by this span. */
    extractText(fullText: string): string {
        if (this.end > fullText.length) {
            throw new RangeError(
                `Text too short (${fullText.length}) for the span (${this})`,
            );
        }
        return fullText.substring(this.start, this.end);
    }

    toString(): string {
        return objToStr(this, Object.keys(this));
    }
}

export { CharSpan };
