/**
 * A tool for [converting][convert] a sorted series of code-point offsets in a [text]
 * to JavaScript string character offsets.
 */
class Cp2JSOffsetConverter {
    private lastCharOffset = 0;
    private lastCodepointOffset = 0;
    private textArray: string[];

    constructor(text: string) {
        this.textArray = [...text];
    }

    /**
     * Converts a codepoint offset in [text] to a JS-char-based-offset.
     *
     * @param codepointOffset The offset in code points.
     * @throws RangeError if the [codepointOffset] is out of bounds of [text],
     * or when its value in this call is smaller than in the previous call.
     */
    convert(codepointOffset: number): number {
        if (codepointOffset < this.lastCodepointOffset) {
            throw new RangeError();
        }

        this.lastCharOffset += this.textArray
            .splice(0, codepointOffset - this.lastCodepointOffset)
            .join("").length;
        this.lastCodepointOffset = codepointOffset;

        return this.lastCharOffset;
    }
}

/**
 * A tool for [converting][convert] a sorted series of JavaScript string character offsets
 * in a [text] to code-point offsets.
 */
class JS2CpOffsetConverter {
    private lastCodepointOffset = 0;
    private lastCharOffset = 0;

    constructor(public text: string) {}

    /**
     * Converts a JavaScript string char-based offset in [text] to a codepoint offset.
     *
     * @param charOffset The offset in JavaScript string characters.
     * @throws RangeError if the [charOffset] is out of bounds of [text],
     * or when its value in this call is smaller than in the previous call.
     */
    convert(charOffset: number): number {
        if (charOffset < this.lastCharOffset) {
            throw new RangeError();
        }

        const cpOff = Array.from(
            this.text.slice(this.lastCharOffset, charOffset),
        ).length;
        this.lastCodepointOffset += cpOff;

        this.lastCharOffset = charOffset;
        return this.lastCodepointOffset;
    }
}

export { Cp2JSOffsetConverter, JS2CpOffsetConverter };
