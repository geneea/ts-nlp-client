/**
 * Universal POS tags.
 * See http://universaldependencies.org/u/pos/all.html
 */
export enum UPos {
    // === Open class ===
    /**  adjective  */
    ADJ = "ADJ",
    /** adverb */
    ADV = "ADV",
    /** interjection */
    INTJ = "INTJ",
    /** noun */
    NOUN = "NOUN",
    /** proper noun */
    PROPN = "PROPN",
    /** verb */
    VERB = "VERB",

    // === Closed class ===
    /**  adposition (preposition or postposition) */
    ADP = "ADP",
    /** auxiliary */
    AUX = "AUX",
    /** coordinating conjunction */
    CCONJ = "CCONJ",
    /** determiner */
    DET = "DET",
    /** numeral */
    NUM = "NUM",
    /** pronoun */
    PRON = "PRON",
    /** particle */
    PART = "PART",
    /** subordinating conjunction */
    SCONJ = "SCONJ",

    /** punctuation */
    PUNCT = "PUNCT",
    /** symbol */
    SYM = "SYM",
    /** other */
    X = "X",
}

/** Conversion to the string value used by the API. */
export function UPosToStr(upos: UPos): string {
    return upos.toString();
}

/** Conversion from a [string][posStr]; with [X] as a fallback for unknown values. */
export function UPosFromStr(posStr: string): UPos {
    return UPos[posStr.toUpperCase() as keyof typeof UPos] ?? UPos.X;
}
