/**
 * Universal syntactic dependencies V2 and V1.
 * See http://universaldependencies.org/u/dep/all.html
 */
export enum UDep {
    // === Nominals - Core ===
    /**  nominal subject  */
    NSUBJ = "nsubj",
    /** object (V2) */
    OBJ = "obj",
    /** indirect object */
    IOBJ = "iobj",

    // === Nominals - Non-core dependents ===
    /**  oblique nominal (V2)  */
    OBL = "obl",
    /** vocative */
    VOCATIVE = "vocative",
    /** expletive */
    EXPL = "expl",
    /** dislocated elements */
    DISLOCATED = "dislocated",

    // === Nominals - Nominal dependents ===
    /**  nominal modifier  */
    NMOD = "nmod",
    /** appositional modifier */
    APPOS = "appos",
    /** numeric modifier */
    NUMMOD = "nummod",

    // === Clauses - Core ===
    /**  clausal subject  */
    CSUBJ = "csubj",
    /** clausal complement */
    CCOMP = "ccomp",
    /** open clausal complement */
    XCOMP = "xcomp",

    // === Clauses - Non-core dependents ===
    ADVCL = "advcl",
    /** adverbial clause modifier */

    // === Clauses - Nominal dependent ===
    ACL = "acl",
    /** clausal modifier of noun (adjectival clause) */

    // === Modifier words - Non-core dependents
    /** adverbial modifier */
    ADVMOD = "advmod",
    /** discourse element */
    DISCOURSE = "discourse",

    // === Modifier words - Nominal dependent ===
    /** adjective modifier */
    AMOD = "amod",

    // === Function Words - Non-core dependents ===
    /** auxiliary */
    AUX = "aux",
    /** */
    COP = "cop",
    /** */
    MARK = "mark",

    // === Function Words - Nominal dependent ===
    /** determiner */
    DET = "det",
    /** classifier (V2) */
    CLF = "clf",
    /** case marking */
    CASE = "case",

    // === Coordination ===
    /** conjunct */
    CONJ = "conj",
    /** coordinating conjunction */
    CC = "cc",

    // === MWE ===
    /**  fixed multiword expression (V2)  */
    FIXED = "fixed",
    /** flat multiword expression (V2) */
    FLAT = "flat",
    /** compound */
    COMPOUND = "compound",

    // === Loose ===
    /**  list  */
    LIST = "list",
    /** parataxis */
    PARATAXIS = "parataxis",

    // === Special ===
    /**  orphan (V2)  */
    ORPHAN = "orphan",
    /** goes with */
    GOESWITH = "goeswith",
    /** overridden disfluency */
    REPARANDUM = "reparandum",

    // === Other ===
    /**  punctuation  */
    PUNCT = "punct",
    /** root */
    ROOT = "root",
    /** unspecidied dependency */
    DEP = "dep",

    // === V1 ===
    /**  passive auxiliary (V1)  */
    AUXPASS = "auxpass",
    /** clausal passive subject (V1) */
    CSUBJPASS = "csubjpass",
    /** direct object (V1; in V2 as obj) */
    DOBJ = "dobj",
    /** foreign words (V1) */
    FOREIGN = "foreign",
    /** multi-word expression (V1) */
    MWE = "mwe",
    /** name (V1) */
    NAME = "name",
    /** negation modifier (V1) */
    NEG = "neg",
    /** passive nominal subject (V1) */
    NSUBJPASS = "nsubjpass",
    /** remnant in ellipsis (V1) */
    REMNANT = "remnant",
}

/** Conversion to the string value used by the API. */
export function UDepToStr(udep: UDep): string {
    return udep.toString();
}

/** Conversion from a [string][depStr]; with [DEP] as a fallback for unknwon values. */
export function UDepFromStr(depStr: string): UDep {
    return UDep[depStr.toUpperCase() as keyof typeof UDep] ?? UDep.DEP;
}
