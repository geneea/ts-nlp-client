/** Checks whether the nums sequence is a subsequence of integers, i.e. n, n+1, n+2, ...  */
export function isSequential(nums: number[]): boolean {
    return nums.length > 1
        ? nums.slice(1).every((x, i) => nums[i] === x - 1)
        : true;
}

/** Checks whether the nums sequence is sorted. */
export function isSorted(nums: number[]): boolean {
    return nums.length > 1 ? nums.slice(1).every((x, i) => nums[i] <= x) : true;
}

/**
 * Converts an object to a human-readable representation.
 * @param obj The target object.
 * @param props A sequence of object properties to include in the output.
 * @returns A human-readable string for the given object.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function objToStr(obj: any, props: string[]): string {
    // TODO: Cover all possible cases once reading from json is supported
    // and we can straightforwardly test the function.
    const name = obj.constructor.name;
    const values: string[] = [];

    for (const p of props) {
        let val = obj[p] ?? null;
        if (val !== null) {
            if (val instanceof String) {
                val = `"${val}"`;
            } else if (Array.isArray(val)) {
                val = JSON.stringify(val);
            }
            values.push(`${p}=${val.toString()}`);
        }
    }
    return `${name}(${values.join(", ")})`;
}

/**
 * Converts a Map instance into an object with corresponding keys and values
 * or null if the map has no entries.
 */
export function serializeMap(
    map: Map<string, unknown>,
): Record<string, unknown> | null {
    return map.size > 0 ? Object.fromEntries(map) : null;
}
