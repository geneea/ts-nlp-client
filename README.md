# README #

Typescript SDK for [Geneea General NLP API](https://help.geneea.com/api_general).

### Installation

Clone the repository and run `npm install` from the command line.

### Building

Run `npm run build` or `./node_modules/.bin/tsc`.

### Testing

Run `npm run test` or `./node_modules/.bin/jest`.